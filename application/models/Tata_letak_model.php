<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tata_letak_model extends CI_Model
{
    function config_data($id)
    {
        $this->db->select('*');
        $this->db->where('id', $id);
        $query = $this->db->get('cms_settings');
        return $query->result_array();
    }

    function update($where, $data, $table)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }
}
