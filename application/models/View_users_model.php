<?php
defined('BASEPATH') or exit('No direct script access allowed');

class View_users_model extends CI_Model
{
    function grafik_petani_data()
    {
        $query = "SELECT DISTINCT `cms_user`.`id`, `cms_user`.`first_name`, (SELECT COUNT(*) FROM `cms_harian`
                  WHERE `id_user` = `cms_user`.`id`) AS `data_harian`
                  FROM `cms_user` ORDER BY `data_harian` DESC";
        return $this->db->query($query);
    }

    function last_data_harian()
    {
        $query = "SELECT `u`.`first_name`, `h`.`tanggal`, `h`.`jumlah_ch`
                  FROM `cms_harian` AS `h` 
                  JOIN `cms_user` AS `u` ON `h`.`id_user` = `u`.`id` 
                  ORDER BY `h`.`id` DESC LIMIT 10";
        return $this->db->query($query);
    }

    function view_data()
    {
        return $this->db->get('cms_user');
    }

    function config_data()
    {
        return $this->db->get('cms_management');
    }

    function total_data_harian()
    {
        $query = "SELECT COUNT(`id`) AS `total_data_harian` FROM `cms_harian`";
        return $this->db->query($query);
    }

    function total_st_pengamatan()
    {
        $query = "SELECT COUNT(`id`) AS `total_st_pengamatan` FROM `cms_st_pengamatan`";
        return $this->db->query($query);
    }

    function total_data_petani()
    {
        $query = "SELECT COUNT(`id`) AS `total_data_petani` FROM `cms_user` WHERE `username` NOT IN('admin')";
        return $this->db->query($query);
    }

    function st_peng_data()
    {
        $query = "SELECT 
                `a`.`id`,
                `a`.`kd_stasiun`,
                `a`.`id_kabkota`,
                `a`.`id_wilayah`,
                `d`.`nm_kabkota`,
                `e`.`nm_kecamatan`,
                `a`.`id_desa`,
                `b`.`nm_desa`,
                `a`.`blok_sawah`,
                `a`.`jenis_tanah`,
                `a`.`latitude`,
                `a`.`longitude`,
                `a`.`luas`,
                `a`.`id_user`,
                `c`.`first_name`,
                `c`.`last_name`,
                `a`.`kelompok_tani`
            FROM `cms_st_pengamatan` AS `a` 
            JOIN `cms_tbl_desa` AS `b` ON `b`.`id` = `a`.`id_desa`
            JOIN `cms_user` AS `c` ON `c`.`id` = `a`.`id_user`
            JOIN `cms_tbl_kabkota` AS `d` ON `d`.`id` = `a`.`id_kabkota`
            JOIN `cms_tbl_kecamatan` AS `e` ON `b`.`id_kecamatan` = `e`.`id`";
        return $this->db->query($query);
    }

    function speng_petani_data($role_id, $username)
    {
        $query = "SELECT 
                `a`.`id`,
                `a`.`kd_stasiun`,
                `a`.`id_kabkota`,
                `a`.`id_wilayah`,
                `d`.`nm_kabkota`,
                `e`.`nm_kecamatan`,
                `a`.`id_desa`,
                `b`.`nm_desa`,
                `a`.`blok_sawah`,
                `a`.`jenis_tanah`,
                `a`.`latitude`,
                `a`.`longitude`,
                `a`.`luas`,
                `a`.`id_user`,
                `c`.`first_name`,
                `c`.`last_name`,
                `a`.`kelompok_tani`
            FROM `cms_st_pengamatan` AS `a` 
            JOIN `cms_tbl_desa` AS `b` ON `b`.`id` = `a`.`id_desa`
            JOIN `cms_user` AS `c` ON `c`.`id` = `a`.`id_user`
            JOIN `cms_tbl_kabkota` AS `d` ON `d`.`id` = `a`.`id_kabkota`
            JOIN `cms_tbl_kecamatan` AS `e` ON `b`.`id_kecamatan` = `e`.`id` WHERE `c`.`role_id` = '$role_id' AND `c`.`username` = '$username'";
        return $this->db->query($query);
    }

    function kabkota_data()
    {
        $query = "SELECT * FROM `cms_tbl_kabkota` WHERE `id` = '3211' OR `id` = '3212' OR `id` = '5203'";
        return $this->db->query($query)->result_array();
    }

    function desa_data($where)
    {
        $query = "SELECT `kecamatan`.`nm_kecamatan`, `desa`.* FROM `cms_tbl_desa` AS `desa` JOIN `cms_tbl_kecamatan` AS `kecamatan` ON `desa`.`id_kecamatan` = `kecamatan`.`id` WHERE `id_kabkota` = '$where'";
        return $this->db->query($query)->result_array();
    }

    function wilayah_data($where)
    {
        return $this->db->get_where('cms_korwil', array('kabkota_id' => $where))->result_array();
    }

    function pengamat_data()
    {
        $role_id = $this->session->userdata('role_id');
        $this->db->select('*');
        $this->db->from('cms_user');
        $this->db->where('role_id !=', 1, false);
        return $this->db->get();
    }

    function edit_stasiun($where, $data, $table)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    function delete_stasiun($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    function edit_musiman($where, $data, $table)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    function delete_musiman($id)
    {
        $this->db->where("id", $id);
        $this->db->delete("cms_musiman");
    }

    function edit_akhir_musim($where, $data, $table)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    function delete_akhir_musim($id)
    {
        $this->db->where("id", $id);
        $this->db->delete("cms_akhir_musim");
    }

    function pestisida_data()
    {
        $query = "SELECT * FROM `cms_pestisida` ORDER BY `tanggal` ASC";
        return $this->db->query($query);
    }

    function delete_pestisida($id)
    {
        $this->db->where("id", $id);
        $this->db->delete("cms_pestisida");
    }

    function pupuk_data()
    {
        $query = "SELECT * FROM `cms_pupuk` ORDER BY `tanggal` ASC";
        return $this->db->query($query);
    }

    function delete_pupuk($id)
    {
        $this->db->where("id", $id);
        $this->db->delete("cms_pupuk");
    }

    function edit_harian($where, $data, $table)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    function delete_harian($id)
    {
        $this->db->where("id", $id);
        $this->db->delete("cms_harian");
    }

    function tahun_data($where)
    {
        $query = "SELECT YEAR(`tanggal`) AS `tahun`, `id_stasiun` FROM `cms_harian` WHERE `id_stasiun` = '$where' GROUP BY YEAR(`tanggal`)";
        return $this->db->query($query)->result_array();
    }

    function dasarian_data_satu($tahun, $id_stasiun)
    {
        $query = "SELECT (CASE 
                    WHEN MONTH(`tanggal`) = '1' THEN 'January' 
                    WHEN MONTH(`tanggal`) = '2' THEN 'Februari' 
                    WHEN MONTH(`tanggal`) = '3' THEN 'Maret' 
                    WHEN MONTH(`tanggal`) = '4' THEN 'April' 
                    WHEN MONTH(`tanggal`) = '5' THEN 'Mei' 
                    WHEN MONTH(`tanggal`) = '6' THEN 'Juni' 
                    WHEN MONTH(`tanggal`) = '7' THEN 'Juli' 
                    WHEN MONTH(`tanggal`) = '8' THEN 'Agustus' 
                    WHEN MONTH(`tanggal`) = '9' THEN 'September' 
                    WHEN MONTH(`tanggal`) = '10' THEN 'Oktober' 
                    WHEN MONTH(`tanggal`) = '11' THEN 'November' 
                    WHEN MONTH(`tanggal`) = '12' THEN 'Desember' 
                END) AS `bulan`, 
                SUM(`jumlah_ch`) AS `dasarian_1`
            FROM `cms_harian` WHERE YEAR(`tanggal`) = '$tahun' AND `id_stasiun` = '$id_stasiun' AND DAY(`tanggal`) BETWEEN 1 AND 10 GROUP BY MONTH(`tanggal`)";
        return $this->db->query($query)->result_array();
    }

    function dasarian_data_dua($tahun, $id_stasiun)
    {
        $query = "SELECT (CASE 
                    WHEN MONTH(`tanggal`) = '1' THEN 'January' 
                    WHEN MONTH(`tanggal`) = '2' THEN 'Februari' 
                    WHEN MONTH(`tanggal`) = '3' THEN 'Maret' 
                    WHEN MONTH(`tanggal`) = '4' THEN 'April' 
                    WHEN MONTH(`tanggal`) = '5' THEN 'Mei' 
                    WHEN MONTH(`tanggal`) = '6' THEN 'Juni' 
                    WHEN MONTH(`tanggal`) = '7' THEN 'Juli' 
                    WHEN MONTH(`tanggal`) = '8' THEN 'Agustus' 
                    WHEN MONTH(`tanggal`) = '9' THEN 'September' 
                    WHEN MONTH(`tanggal`) = '10' THEN 'Oktober' 
                    WHEN MONTH(`tanggal`) = '11' THEN 'November' 
                    WHEN MONTH(`tanggal`) = '12' THEN 'Desember' 
                END) AS `bulan`, 
                SUM(`jumlah_ch`) AS `dasarian_2`
            FROM `cms_harian` WHERE YEAR(`tanggal`) = '$tahun' AND `id_stasiun` = '$id_stasiun' AND DAY(`tanggal`) BETWEEN 10 AND 20 GROUP BY MONTH(`tanggal`)";
        return $this->db->query($query)->result_array();
    }

    function dasarian_data_tiga($tahun, $id_stasiun)
    {
        $query = "SELECT (CASE 
                    WHEN MONTH(`tanggal`) = '1' THEN 'January' 
                    WHEN MONTH(`tanggal`) = '2' THEN 'Februari' 
                    WHEN MONTH(`tanggal`) = '3' THEN 'Maret' 
                    WHEN MONTH(`tanggal`) = '4' THEN 'April' 
                    WHEN MONTH(`tanggal`) = '5' THEN 'Mei' 
                    WHEN MONTH(`tanggal`) = '6' THEN 'Juni' 
                    WHEN MONTH(`tanggal`) = '7' THEN 'Juli' 
                    WHEN MONTH(`tanggal`) = '8' THEN 'Agustus' 
                    WHEN MONTH(`tanggal`) = '9' THEN 'September' 
                    WHEN MONTH(`tanggal`) = '10' THEN 'Oktober' 
                    WHEN MONTH(`tanggal`) = '11' THEN 'November' 
                    WHEN MONTH(`tanggal`) = '12' THEN 'Desember' 
                END) AS `bulan`, 
                SUM(`jumlah_ch`) AS `dasarian_3`
            FROM `cms_harian` WHERE YEAR(`tanggal`) = '$tahun' AND `id_stasiun` = '$id_stasiun' AND DAY(`tanggal`) BETWEEN 20 AND 31 GROUP BY MONTH(`tanggal`)";
        return $this->db->query($query)->result_array();
    }

    function dasarian_data_total($tahun, $id_stasiun)
    {
        $query = "SELECT (CASE 
                    WHEN MONTH(`tanggal`) = '1' THEN 'January' 
                    WHEN MONTH(`tanggal`) = '2' THEN 'Februari' 
                    WHEN MONTH(`tanggal`) = '3' THEN 'Maret' 
                    WHEN MONTH(`tanggal`) = '4' THEN 'April' 
                    WHEN MONTH(`tanggal`) = '5' THEN 'Mei' 
                    WHEN MONTH(`tanggal`) = '6' THEN 'Juni' 
                    WHEN MONTH(`tanggal`) = '7' THEN 'Juli' 
                    WHEN MONTH(`tanggal`) = '8' THEN 'Agustus' 
                    WHEN MONTH(`tanggal`) = '9' THEN 'September' 
                    WHEN MONTH(`tanggal`) = '10' THEN 'Oktober' 
                    WHEN MONTH(`tanggal`) = '11' THEN 'November' 
                    WHEN MONTH(`tanggal`) = '12' THEN 'Desember' 
                END) AS `bulan`, 
                SUM(`jumlah_ch`) AS `total`
            FROM `cms_harian` WHERE YEAR(`tanggal`) = '$tahun' AND `id_stasiun` = '$id_stasiun' GROUP BY MONTH(`tanggal`)";
        return $this->db->query($query)->result_array();
    }

    function grafik_ch_bulan($tahun, $id_stasiun)
    {
        $query = "SELECT (CASE 
                            WHEN MONTH(`tanggal`) = '1' THEN 'January' 
                            WHEN MONTH(`tanggal`) = '2' THEN 'Februari' 
                            WHEN MONTH(`tanggal`) = '3' THEN 'Maret' 
                            WHEN MONTH(`tanggal`) = '4' THEN 'April' 
                            WHEN MONTH(`tanggal`) = '5' THEN 'Mei' 
                            WHEN MONTH(`tanggal`) = '6' THEN 'Juni' 
                            WHEN MONTH(`tanggal`) = '7' THEN 'Juli' 
                            WHEN MONTH(`tanggal`) = '8' THEN 'Agustus' 
                            WHEN MONTH(`tanggal`) = '9' THEN 'September' 
                            WHEN MONTH(`tanggal`) = '10' THEN 'Oktober' 
                            WHEN MONTH(`tanggal`) = '11' THEN 'November' 
                            WHEN MONTH(`tanggal`) = '12' THEN 'Desember' 
                        END) AS `bulan_name`, 
                        MONTH(`tanggal`) AS `bulan`
                FROM `cms_harian` WHERE YEAR(`tanggal`) = '$tahun' AND `id_stasiun` = '$id_stasiun' 
                GROUP BY `bulan`, `bulan_name`";
        return $this->db->query($query)->result_array();
    }

    function grafik_ch_tanggal($tahun, $id_stasiun, $bulan)
    {
        $query = "SELECT DAY(`tanggal`) AS `tanggal`, `jumlah_ch`
        FROM `cms_harian` WHERE YEAR(`tanggal`) = '$tahun' AND `id_stasiun` = '$id_stasiun' AND MONTH(`tanggal`) = '$bulan'";
        return $this->db->query($query)->result_array();
    }

    function box_data_table($id_stasiun)
    {
        $query = "SELECT YEAR(`tanggal`) AS `tahun`,
                SUM(CASE WHEN MONTH(`tanggal`) = '1'  AND `id_stasiun` = '3' THEN `cms_harian`.`jumlah_ch` ELSE 0 END) AS `jan`,
                SUM(CASE WHEN MONTH(`tanggal`) = '2'  AND `id_stasiun` = '3' THEN `cms_harian`.`jumlah_ch` ELSE 0 END) AS `feb`,
                SUM(CASE WHEN MONTH(`tanggal`) = '3'  AND `id_stasiun` = '3' THEN `cms_harian`.`jumlah_ch` ELSE 0 END) AS `mar`,
                SUM(CASE WHEN MONTH(`tanggal`) = '4'  AND `id_stasiun` = '3' THEN `cms_harian`.`jumlah_ch` ELSE 0 END) AS `apr`,
                SUM(CASE WHEN MONTH(`tanggal`) = '5'  AND `id_stasiun` = '3' THEN `cms_harian`.`jumlah_ch` ELSE 0 END) AS `mei`,
                SUM(CASE WHEN MONTH(`tanggal`) = '6'  AND `id_stasiun` = '3' THEN `cms_harian`.`jumlah_ch` ELSE 0 END) AS `jun`,
                SUM(CASE WHEN MONTH(`tanggal`) = '7'  AND `id_stasiun` = '3' THEN `cms_harian`.`jumlah_ch` ELSE 0 END) AS `jul`,
                SUM(CASE WHEN MONTH(`tanggal`) = '8'  AND `id_stasiun` = '3' THEN `cms_harian`.`jumlah_ch` ELSE 0 END) AS `aug`,
                SUM(CASE WHEN MONTH(`tanggal`) = '9'  AND `id_stasiun` = '3' THEN `cms_harian`.`jumlah_ch` ELSE 0 END) AS `sep`,
                SUM(CASE WHEN MONTH(`tanggal`) = '10'  AND `id_stasiun` = '3' THEN `cms_harian`.`jumlah_ch` ELSE 0 END) AS `okt`,
                SUM(CASE WHEN MONTH(`tanggal`) = '11'  AND `id_stasiun` = '3' THEN `cms_harian`.`jumlah_ch` ELSE 0 END) AS `nov`,
                SUM(CASE WHEN MONTH(`tanggal`) = '12'  AND `id_stasiun` = '3' THEN `cms_harian`.`jumlah_ch` ELSE 0 END) AS `des`
        FROM `cms_harian` WHERE `id_stasiun` = '3' GROUP BY YEAR(`tanggal`)";
        return $this->db->query($query)->result_array();
    }
}
