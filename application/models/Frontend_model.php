<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Frontend_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
    }

    function view_data($id)
    {
        $this->db->select('*');
        $this->db->where('id', $id);
        $query = $this->db->get('cms_settings');
        return $query->row();
    }
}
