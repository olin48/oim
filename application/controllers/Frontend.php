<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Frontend extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        // $this->load->config('email');
        $this->load->library('form_validation');
        $this->load->library('session');
        // $this->load->library('email');
        $this->load->model('Frontend_model', 'frontend');
        $this->load->model('Management_model', 'management');
        $this->load->model('Kerjasama_model', 'kerjasama');
        $this->load->model('Client_model', 'client');
        $this->load->model('Service_model', 'service');
        $this->load->model('Client_testi_model', 'client_testi');
        $this->load->model('Team_model', 'team');
        $this->load->model('Gallery_model', 'gallery');
    }

    public function index()
    {
        $data = $this->siteSettings();
        $data['home'] = $this->frontend->view_data("11");
        $data['about'] = $this->frontend->view_data("12");
        $data['team_settings'] = $this->frontend->view_data("15");
        $data['service_settings'] = $this->frontend->view_data("16");
        $data['gallery_settings'] = $this->frontend->view_data("17");
        $data['kerjasama'] = $this->kerjasama->view_data();
        $data['client'] = $this->client->view_data();
        $data['team'] = $this->team->view_data();
        $data['client_testi'] = $this->client_testi->view_data();
        $data['service'] = $this->service->view_data();
        $data['gallery'] = $this->gallery->view_data();
        $this->load->view('frontend/theme/header', $data);
        $this->load->view('frontend/home');
        $this->load->view('frontend/theme/footer', $data);
    }

    public function siteSettings()
    {
        $data['is_login'] = $this->session->userdata('id_student');
        $data['config'] = $this->management->config_data()->row();
        $data['footer'] = $this->frontend->view_data("4");
        $data['menu'] = $this->frontend->view_data("5");
        $data['logo'] = $this->frontend->view_data("6");
        $data['footer1'] = $this->frontend->view_data("7");
        $data['footer2'] = $this->frontend->view_data("8");
        $data['footer3'] = $this->frontend->view_data("9");
        $data['footer4'] = $this->frontend->view_data("10");
        return $data;
    }
}
