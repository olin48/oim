<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('view_users_model');
        $this->load->library('session');
        $this->load->model('tata_letak_model', 'tata_let');
        $this->load->model('kerjasama_model', 'kerjasama');
        $this->load->model('client_model', 'client');
        $this->load->model('service_model', 'service');
        $this->load->model('client_testi_model', 'client_testi');
        $this->load->model('team_model', 'team');
        $this->load->model('gallery_model', 'gallery');
        is_logged_in();
    }

    public function siteSettings()
    {
        $data['footer'] = $this->tata_let->config_data("4");
        $data['menu_header'] = $this->tata_let->config_data("5");
        $data['logo'] = $this->tata_let->config_data("6");
        $data['column_1'] = $this->tata_let->config_data("7");
        $data['column_2'] = $this->tata_let->config_data("8");
        $data['column_3'] = $this->tata_let->config_data("9");
        $data['column_4'] = $this->tata_let->config_data("10");
        return $data;
    }

    public function dashboard()
    {
        $config['web'] = $this->view_users_model->config_data()->result_array();
        $data['menu_title'] = "";
        $data['url'] = "admin/dashboard";
        $this->load->view('templates/header', $config);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/breadcumb', $data);
        $this->load->view('cms/dashboard');
        $this->load->view('templates/footer', $config);
    }

    public function tata_letak()
    {
        $config['web'] = $this->view_users_model->config_data()->result_array();
        $data['header'] = $this->view_users_model->config_data()->row();
        $data = $this->siteSettings();
        $data['menu_title'] = "Tata Letak";
        $data['url'] = "admin/tata_letak";
        $this->load->view('templates/header', $config);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/breadcumb', $data);
        $this->load->view('cms/tata_letak', $data);
        $this->load->view('templates/footer', $config);
    }

    public function home_settings()
    {
        $config['web'] = $this->view_users_model->config_data()->result_array();
        $data = $this->siteSettings();
        $data['home_settings'] = $this->tata_let->config_data("11");
        $data['menu_title'] = "Home Settings";
        $data['url'] = "admin/home_settings";
        $this->load->view('templates/header', $config);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/breadcumb', $data);
        $this->load->view('cms/home_settings', $data);
        $this->load->view('templates/footer', $config);
    }

    public function about_settings()
    {
        $config['web'] = $this->view_users_model->config_data()->result_array();
        $data = $this->siteSettings();
        $data['about_settings'] = $this->tata_let->config_data("12");
        $data['menu_title'] = "About Settings";
        $data['url'] = "admin/about_settings";
        $this->load->view('templates/header', $config);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/breadcumb', $data);
        $this->load->view('cms/about_settings', $data);
        $this->load->view('templates/footer', $config);
    }

    public function client_settings()
    {
        $config['web'] = $this->view_users_model->config_data()->result_array();
        $data['header'] = $this->view_users_model->config_data()->row();
        $data = $this->siteSettings();
        $data['menu_title'] = "Client Settings";
        $data['url'] = "admin/client_settings";
        $this->load->view('templates/header', $config);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/breadcumb', $data);
        $this->load->view('cms/client_settings', $data);
        $this->load->view('templates/footer', $config);
        $this->load->view('templates/script_client_settings');
    }

    public function add_client_setting()
    {
        $this->form_validation->set_rules('nama_client_add', 'Nama Client', 'required|trim');
        $this->form_validation->set_rules('link_url_client_add', 'Link URL', 'required|trim');
        $this->form_validation->set_rules('client_status_id_add', 'Status', 'required|trim');

        $data = [
            'nama_client' => $this->input->post('nama_client_add'),
            'link_url' => $this->input->post('link_url_client_add'),
            'image' => $this->_uploadImage("logo_client_"),
            'is_active' => $this->input->post('client_status_id_add')
        ];

        if ($this->form_validation->run() == true) {
            $this->db->insert('cms_client_settings', $data);
            $this->session->set_flashdata('message-client', '<div class="alert alert-success" role="alert">Penambahan data client sukses!</div>');
            redirect('admin/client_settings');
        } else {
            redirect('admin/client_settings');
        }
    }

    public function get_data_client()
    {
        $list = $this->client->get_datatables();
        $data = array();
        $no = @$_POST['start'];
        foreach ($list as $item) {
            $no++;
            $row = array();
            $row[] = $no . ".";
            $row[] = $item->nama_client;
            $row[] = $item->link_url;
            $row[] = '<img src="' . base_url("assets/uploads/" . $item->image) . '" height="30px"/>';
            if ($item->is_active == 0) {
                $row[] = '<center?><td align="center"><span class="label label-warning">Not Active</span></td></center>';
            } else if ($item->is_active == 1) {
                $row[] = '<center><td align="center"><span class="label label-success">Active</span></td></center>';
            }
            $row[] = '<center><button class="btn-link d-edit" onclick="edit_client_setting(' . "'" . $item->id . "'" . ')" title="Edit"><i class="fa fa-edit" style="color:green; font-size:12px;"></i></button><button class="btn-link d-delete" onclick="delete_client_setting(' . "'" . $item->id . "'" . ')" title="Hapus"><i class="fontello-trash-2" style="color:red; font-size:12px;"></i></button></center>';
            $data[] = $row;
        }
        $output = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->client->count_all(),
            "recordsFiltered" => $this->client->count_filtered(),
            "data" => $data,
        );
        // output to json format
        echo json_encode($output);
    }

    public function edit_data_client_setting($id)
    {
        is_logged_in();
        $data = $this->client->get_by_id($id);
        echo json_encode($data);
    }

    public function update_client_settings()
    {
        $this->form_validation->set_rules('nama_client_edit', 'Nama Client', 'required|trim');
        $this->form_validation->set_rules('link_url_client_edit', 'Link URL', 'required|trim');
        $this->form_validation->set_rules('client_status_id_edit', 'Status', 'required|trim');
        $id = $this->input->post('client_id_edit');
        $where = ['id' => $id];
        //load date helper
        $this->load->helper('date');
        $format = "%Y-%m-%d %h:%i";

        if (!empty($_FILES["logo"]["name"])) {
            $this->_deleteImage($this->input->post('old_logo'));
            $logo = $this->_uploadImage("logo_client_");
        } else {
            $logo = $this->input->post('old_logo');
        }

        $data = [
            'nama_client' => $this->input->post('nama_client_edit'),
            'link_url' => $this->input->post('link_url_client_edit'),
            'image' => $logo,
            'is_active' => $this->input->post('client_status_id_edit'),
            'updated_date' => @mdate($format)
        ];

        if ($this->form_validation->run() == true) {
            $this->client->edit_client($where, $data, 'cms_client_settings');
            $this->session->set_flashdata('message-client', '<div class="alert alert-success" role="alert">Perubahan data client sukses!</div>');
            redirect('admin/client_settings');
        } else {
            redirect('admin/client_settings');
        }
    }

    public function delete_client_settings($id)
    {
        $this->client->delete_client($id);
        echo 'Data submenu berhasil dihapus!';
    }

    public function kerja_sama_settings()
    {
        $config['web'] = $this->view_users_model->config_data()->result_array();
        $data['header'] = $this->view_users_model->config_data()->row();
        $data = $this->siteSettings();
        $data['menu_title'] = "Kerja Sama Settings";
        $data['url'] = "admin/kerja_sama_settings";
        $this->load->view('templates/header', $config);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/breadcumb', $data);
        $this->load->view('cms/kerja_sama_settings', $data);
        $this->load->view('templates/footer', $config);
        $this->load->view('templates/script_kerja_sama_settings');
    }

    public function add_kerjasama_setting()
    {
        $this->form_validation->set_rules('nama_kerjasama_add', 'Nama Kerja Sama', 'required|trim');
        $this->form_validation->set_rules('link_url_kerjasama_add', 'Link URL', 'required|trim');
        $this->form_validation->set_rules('kerjasama_status_id_add', 'Status', 'required|trim');

        $data = [
            'nama_kerjasama' => $this->input->post('nama_kerjasama_add'),
            'link_url' => $this->input->post('link_url_kerjasama_add'),
            'image' => $this->_uploadImage("logo_kerjasama_"),
            'is_active' => $this->input->post('kerjasama_status_id_add')
        ];

        if ($this->form_validation->run() == true) {
            $this->db->insert('cms_kerjasama_settings', $data);
            $this->session->set_flashdata('message-kerjasama', '<div class="alert alert-success" role="alert">Penambahan data kerja sama sukses!</div>');
            redirect('admin/kerja_sama_settings');
        } else {
            redirect('admin/kerja_sama_settings');
        }
    }

    public function get_data_kerjasama()
    {
        $list = $this->kerjasama->get_datatables();
        $data = array();
        $no = @$_POST['start'];
        foreach ($list as $item) {
            $no++;
            $row = array();
            $row[] = $no . ".";
            $row[] = $item->nama_kerjasama;
            $row[] = $item->link_url;
            $row[] = '<img src="' . base_url("assets/uploads/" . $item->image) . '" height="30px"/>';
            if ($item->is_active == 0) {
                $row[] = '<center?><td align="center"><span class="label label-warning">Not Active</span></td></center>';
            } else if ($item->is_active == 1) {
                $row[] = '<center><td align="center"><span class="label label-success">Active</span></td></center>';
            }
            $row[] = '<center><button class="btn-link d-edit" onclick="edit_kerjasama_setting(' . "'" . $item->id . "'" . ')" title="Edit"><i class="fa fa-edit" style="color:green; font-size:12px;"></i></button><button class="btn-link d-delete" onclick="delete_kerjasama_setting(' . "'" . $item->id . "'" . ')" title="Hapus"><i class="fontello-trash-2" style="color:red; font-size:12px;"></i></button></center>';
            $data[] = $row;
        }
        $output = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->kerjasama->count_all(),
            "recordsFiltered" => $this->kerjasama->count_filtered(),
            "data" => $data,
        );
        // output to json format
        echo json_encode($output);
    }

    public function edit_data_kerjasama_setting($id)
    {
        is_logged_in();
        $data = $this->kerjasama->get_by_id($id);
        echo json_encode($data);
    }

    public function update_kerjasama_settings()
    {
        $this->form_validation->set_rules('nama_kerjasama_edit', 'Nama Kerja Sama', 'required|trim');
        $this->form_validation->set_rules('link_url_kerjasama_edit', 'Link URL', 'required|trim');
        $this->form_validation->set_rules('kerjasama_status_id_edit', 'Status', 'required|trim');
        $id = $this->input->post('kerjasama_id_edit');
        $where = ['id' => $id];
        //load date helper
        $this->load->helper('date');
        $format = "%Y-%m-%d %h:%i";

        if (!empty($_FILES["logo"]["name"])) {
            $this->_deleteImage($this->input->post('old_logo'));
            $logo = $this->_uploadImage("logo_kerjasama_");
        } else {
            $logo = $this->input->post('old_logo');
        }

        $data = [
            'nama_kerjasama' => $this->input->post('nama_kerjasama_edit'),
            'link_url' => $this->input->post('link_url_kerjasama_edit'),
            'image' => $logo,
            'is_active' => $this->input->post('kerjasama_status_id_edit'),
            'updated_date' => @mdate($format)
        ];

        if ($this->form_validation->run() == true) {
            $this->kerjasama->edit_kerjasama($where, $data, 'cms_kerjasama_settings');
            $this->session->set_flashdata('message-kerjasama', '<div class="alert alert-success" role="alert">Perubahan data kerja sama sukses!</div>');
            redirect('admin/kerja_sama_settings');
        } else {
            redirect('admin/kerja_sama_settings');
        }
    }

    public function delete_kerjasama_settings($id)
    {
        $this->kerjasama->delete_kerjasama($id);
        echo 'Data kerjasama berhasil dihapus!';
    }

    public function service_settings()
    {
        $config['web'] = $this->view_users_model->config_data()->result_array();
        $data['header'] = $this->view_users_model->config_data()->row();
        $data['service_settings'] = $this->tata_let->config_data("16");
        $this->siteSettings();
        $data['menu_title'] = "Service Settings";
        $data['url'] = "admin/service_settings";
        $this->load->view('templates/header', $config);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/breadcumb', $data);
        $this->load->view('cms/service_settings', $data);
        $this->load->view('templates/footer', $config);
        $this->load->view('templates/script_service_settings');
    }

    public function update_title_service_settings()
    {
        $this->form_validation->set_rules('content-service', 'Content', 'required|trim');
        $where = ['id' => "16"];
        //load date helper
        $this->load->helper('date');
        $format = "%Y-%m-%d %h:%i";

        $data = [
            'judul' => $this->input->post('title-service'),
            'content' => $this->input->post('content-service'),
            'updated_date' => @mdate($format)
        ];

        if ($this->form_validation->run() == true) {
            $this->tata_let->update($where, $data, 'cms_settings');
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Perubahan data title service sukses!</div>');
            redirect('admin/service_settings');
        } else {
            redirect('admin/service_settings');
        }
    }

    public function add_service_setting()
    {
        $this->form_validation->set_rules('service_add', 'Nama Service', 'required|trim');
        $this->form_validation->set_rules('description_add', 'Description Service', 'required|trim');

        $data = [
            'service_name' => $this->input->post('service_add'),
            'service_desc' => $this->input->post('description_add'),
            'service_logo' => $this->_uploadImage("logo_service_"),
            'service_url' => $this->input->post('url_add'),
            'is_active' => $this->input->post('service_id_add')
        ];

        if ($this->form_validation->run() == true) {
            $this->db->insert('cms_service_settings', $data);
            $this->session->set_flashdata('message-service', '<div class="alert alert-success" role="alert">Penambahan data service sukses!</div>');
            redirect('admin/service_settings');
        } else {
            redirect('admin/service_settings');
        }
    }

    public function get_data_service()
    {
        $list = $this->service->get_datatables();
        $data = array();
        $no = @$_POST['start'];
        foreach ($list as $item) {
            $no++;
            $row = array();
            $row[] = $no . ".";
            $row[] = $item->service_name;
            $row[] = $item->service_desc;
            $row[] = '<img src="' . base_url("assets/uploads/" . $item->service_logo) . '" height="30px"/>';
            $row[] = $item->service_url;
            if ($item->is_active == 0) {
                $row[] = '<center?><td align="center"><span class="label label-warning">Not Active</span></td></center>';
            } else if ($item->is_active == 1) {
                $row[] = '<center><td align="center"><span class="label label-success">Active</span></td></center>';
            }
            $row[] = '<center><button class="btn-link d-edit" onclick="edit_service_setting(' . "'" . $item->id . "'" . ')" title="Edit"><i class="fa fa-edit" style="color:green; font-size:12px;"></i></button><button class="btn-link d-delete" onclick="delete_service_setting(' . "'" . $item->id . "'" . ')" title="Hapus"><i class="fontello-trash-2" style="color:red; font-size:12px;"></i></button></center>';
            $data[] = $row;
        }
        $output = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->service->count_all(),
            "recordsFiltered" => $this->service->count_filtered(),
            "data" => $data,
        );
        // output to json format
        echo json_encode($output);
    }

    public function edit_data_service_setting($id)
    {
        is_logged_in();
        $data = $this->service->get_by_id($id);
        echo json_encode($data);
    }

    public function update_service_settings()
    {
        $this->form_validation->set_rules('service_edit', 'Nama Service', 'required|trim');
        $this->form_validation->set_rules('description_edit', 'Description Service', 'required|trim');

        $id = $this->input->post('id_edit');
        $where = ['id' => $id];
        //load date helper
        $this->load->helper('date');
        $format = "%Y-%m-%d %h:%i";

        if (!empty($_FILES["logo"]["name"])) {
            $this->_deleteImage($this->input->post('old_logo'));
            $logo = $this->_uploadImage("logo_service_");
        } else {
            $logo = $this->input->post('old_logo');
        }

        $data = [
            'service_name' => $this->input->post('service_edit'),
            'service_desc' => $this->input->post('description_edit'),
            'service_logo' => $logo,
            'service_url' => $this->input->post('url_edit'),
            'is_active' => $this->input->post('service_id_edit'),
            'updated_date' => @mdate($format)
        ];

        if ($this->form_validation->run() == true) {
            $this->service->edit_service($where, $data, 'cms_service_settings');
            $this->session->set_flashdata('message-service', '<div class="alert alert-success" role="alert">Perubahan data service sukses!</div>');
            redirect('admin/service_settings');
        } else {
            redirect('admin/service_settings');
        }
    }

    public function delete_service_settings($id)
    {
        $this->service->delete_service($id);
        echo 'Data submenu berhasil dihapus!';
    }

    public function client_testi_settings()
    {
        $config['web'] = $this->view_users_model->config_data()->result_array();
        $data['header'] = $this->view_users_model->config_data()->row();
        $this->siteSettings();
        $data['menu_title'] = "Client Testi Settings";
        $data['url'] = "admin/client_testi_settings";
        $this->load->view('templates/header', $config);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/breadcumb', $data);
        $this->load->view('cms/client_testi_settings', $data);
        $this->load->view('templates/footer', $config);
        $this->load->view('templates/script_client_testi_settings');
    }

    public function get_data_client_testi()
    {
        $list = $this->client_testi->get_datatables();
        $data = array();
        $no = @$_POST['start'];
        foreach ($list as $item) {
            $no++;
            $row = array();
            $row[] = $no . ".";
            $row[] = $item->nama;
            $row[] = $item->jabatan;
            $row[] = '<img src="' . base_url("assets/uploads/" . $item->photo) . '" height="30px"/>';
            $row[] = $item->komentar;
            if ($item->is_active == 0) {
                $row[] = '<center?><td align="center"><span class="label label-warning">Not Active</span></td></center>';
            } else if ($item->is_active == 1) {
                $row[] = '<center><td align="center"><span class="label label-success">Active</span></td></center>';
            }
            $row[] = '<center><button class="btn-link d-edit" onclick="edit_client_testi_setting(' . "'" . $item->id . "'" . ')" title="Edit"><i class="fa fa-edit" style="color:green; font-size:12px;"></i></button><button class="btn-link d-delete" onclick="delete_client_testi_setting(' . "'" . $item->id . "'" . ')" title="Hapus"><i class="fontello-trash-2" style="color:red; font-size:12px;"></i></button></center>';
            $data[] = $row;
        }
        $output = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->client_testi->count_all(),
            "recordsFiltered" => $this->client_testi->count_filtered(),
            "data" => $data,
        );
        // output to json format
        echo json_encode($output);
    }

    public function add_client_testi_setting()
    {
        $this->form_validation->set_rules('client_testi_add', 'Client Testi', 'required|trim');
        $this->form_validation->set_rules('jabatan_testi_add', 'Jabatan Client', 'required|trim');

        $data = [
            'nama' => $this->input->post('client_testi_add'),
            'jabatan' => $this->input->post('jabatan_testi_add'),
            'photo' => $this->_uploadImage("logo_client_testi_"),
            'komentar' => $this->input->post('komentar_testi_add'),
            'is_active' => $this->input->post('client_testi_status_id_add')
        ];

        if ($this->form_validation->run() == true) {
            $this->db->insert('cms_client_testi_settings', $data);
            $this->session->set_flashdata('message-client-testi', '<div class="alert alert-success" role="alert">Penambahan data client testi sukses!</div>');
            redirect('admin/client_testi_settings');
        } else {
            redirect('admin/client_testi_settings');
        }
    }

    public function edit_data_client_testi_setting($id)
    {
        is_logged_in();
        $data = $this->client_testi->get_by_id($id);
        echo json_encode($data);
    }

    public function update_client_testi_settings()
    {
        $this->form_validation->set_rules('client_testi_edit', 'Client Testi', 'required|trim');
        $this->form_validation->set_rules('jabatan_testi_edit', 'Jabatan Client', 'required|trim');

        $id = $this->input->post('client_testi_id_edit');
        $where = ['id' => $id];
        //load date helper
        $this->load->helper('date');
        $format = "%Y-%m-%d %h:%i";

        if (!empty($_FILES["logo"]["name"])) {
            $this->_deleteImage($this->input->post('old_logo'));
            $logo = $this->_uploadImage("logo_client_testi_");
        } else {
            $logo = $this->input->post('old_logo');
        }

        $data = [
            'nama' => $this->input->post('client_testi_edit'),
            'jabatan' => $this->input->post('jabatan_testi_edit'),
            'photo' => $logo,
            'komentar' => $this->input->post('komentar_testi_edit'),
            'is_active' => $this->input->post('client_testi_status_id_edit')
        ];


        if ($this->form_validation->run() == true) {
            $this->client_testi->edit_client_testi($where, $data, 'cms_client_testi_settings');
            $this->session->set_flashdata('message-client-testi', '<div class="alert alert-success" role="alert">Perubahan data client testi sukses!</div>');
            redirect('admin/client_testi_settings');
        } else {
            redirect('admin/client_testi_settings');
        }
    }

    public function delete_client_testi_settings($id)
    {
        $this->client_testi->delete_client_testi($id);
        echo 'Data submenu berhasil dihapus!';
    }

    public function team_settings()
    {
        $config['web'] = $this->view_users_model->config_data()->result_array();
        $data['header'] = $this->view_users_model->config_data()->row();
        $data['team_settings'] = $this->tata_let->config_data("15");
        $this->siteSettings();
        $data['menu_title'] = "Team Settings";
        $data['url'] = "admin/team_settings";
        $this->load->view('templates/header', $config);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/breadcumb', $data);
        $this->load->view('cms/team_settings', $data);
        $this->load->view('templates/footer', $config);
        $this->load->view('templates/script_team_settings');
    }

    public function update_title_team_settings()
    {
        $this->form_validation->set_rules('content-team', 'Content', 'required|trim');
        $where = ['id' => "15"];
        //load date helper
        $this->load->helper('date');
        $format = "%Y-%m-%d %h:%i";

        $data = [
            'judul' => $this->input->post('title-team'),
            'content' => $this->input->post('content-team'),
            'updated_date' => @mdate($format)
        ];

        if ($this->form_validation->run() == true) {
            $this->tata_let->update($where, $data, 'cms_settings');
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Perubahan data title team sukses!</div>');
            redirect('admin/team_settings');
        } else {
            redirect('admin/team_settings');
        }
    }

    public function add_team_setting()
    {
        $this->form_validation->set_rules('nama_team_add', 'Nama Team', 'required|trim');
        $this->form_validation->set_rules('jabatan_team_add', 'Jabatan Team', 'required|trim');

        $data = [
            'nama' => $this->input->post('nama_team_add'),
            'jabatan' => $this->input->post('jabatan_team_add'),
            'sosmed_twitter' => $this->input->post('sosmed_twitter_add'),
            'sosmed_facebook' => $this->input->post('sosmed_facebook_add'),
            'sosmed_instagram' => $this->input->post('sosmed_instagram_add'),
            'sosmed_linkedin' => $this->input->post('sosmed_linkedin_add'),
            'photo' => $this->_uploadImage("logo_team_"),
            'is_active' => $this->input->post('team_status_id_add')
        ];

        if ($this->form_validation->run() == true) {
            $this->db->insert('cms_team_settings', $data);
            $this->session->set_flashdata('message-client', '<div class="alert alert-success" role="alert">Penambahan data team sukses!</div>');
            redirect('admin/team_settings');
        } else {
            redirect('admin/team_settings');
        }
    }

    public function get_data_team()
    {
        $list = $this->team->get_datatables();
        $data = array();
        $no = @$_POST['start'];
        foreach ($list as $item) {
            $no++;
            $row = array();
            $row[] = $no . ".";
            $row[] = $item->nama;
            $row[] = $item->jabatan;
            $row[] = $item->sosmed_twitter;
            $row[] = $item->sosmed_facebook;
            $row[] = $item->sosmed_instagram;
            $row[] = $item->sosmed_linkedin;
            $row[] = '<img src="' . base_url("assets/uploads/" . $item->photo) . '" height="30px"/>';
            if ($item->is_active == 0) {
                $row[] = '<center?><td align="center"><span class="label label-warning">Not Active</span></td></center>';
            } else if ($item->is_active == 1) {
                $row[] = '<center><td align="center"><span class="label label-success">Active</span></td></center>';
            }
            $row[] = '<center><button class="btn-link d-edit" onclick="edit_team_setting(' . "'" . $item->id . "'" . ')" title="Edit"><i class="fa fa-edit" style="color:green; font-size:12px;"></i></button><button class="btn-link d-delete" onclick="delete_team_setting(' . "'" . $item->id . "'" . ')" title="Hapus"><i class="fontello-trash-2" style="color:red; font-size:12px;"></i></button></center>';
            $data[] = $row;
        }
        $output = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->client->count_all(),
            "recordsFiltered" => $this->client->count_filtered(),
            "data" => $data,
        );
        // output to json format
        echo json_encode($output);
    }

    public function gallery_settings()
    {
        $config['web'] = $this->view_users_model->config_data()->result_array();
        $data['header'] = $this->view_users_model->config_data()->row();
        $data['gallery_settings'] = $this->tata_let->config_data("17");
        $this->siteSettings();
        $data['menu_title'] = "Gallery Settings";
        $data['url'] = "admin/gallery_settings";
        $this->load->view('templates/header', $config);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/breadcumb', $data);
        $this->load->view('cms/gallery_settings', $data);
        $this->load->view('templates/footer', $config);
        $this->load->view('templates/script_gallery_settings');
    }

    public function update_title_gallery_settings()
    {
        $this->form_validation->set_rules('content-gallery', 'Content', 'required|trim');
        $where = ['id' => "17"];
        //load date helper
        $this->load->helper('date');
        $format = "%Y-%m-%d %h:%i";

        $data = [
            'judul' => $this->input->post('title-gallery'),
            'content' => $this->input->post('content-gallery'),
            'updated_date' => @mdate($format)
        ];

        if ($this->form_validation->run() == true) {
            $this->tata_let->update($where, $data, 'cms_settings');
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Perubahan data title gallery sukses!</div>');
            redirect('admin/gallery_settings');
        } else {
            redirect('admin/gallery_settings');
        }
    }

    public function add_gallery_settings()
    {
        $this->form_validation->set_rules('nama_gallery_add', 'Nama Foto', 'required|trim');

        $data = [
            'nama' => $this->input->post('nama_gallery_add'),
            'keterangan' => $this->input->post('ket_gallery_add'),
            'image' => $this->_uploadImage("logo_gallery_"),
            'is_active' => $this->input->post('gallery_status_id_add')
        ];

        if ($this->form_validation->run() == true) {
            $this->db->insert('cms_gallery_settings', $data);
            $this->session->set_flashdata('message-client', '<div class="alert alert-success" role="alert">Penambahan data gallery sukses!</div>');
            redirect('admin/gallery_settings');
        } else {
            redirect('admin/gallery_settings');
        }
    }

    public function get_data_gallery()
    {
        $list = $this->gallery->get_datatables();
        $data = array();
        $no = @$_POST['start'];
        foreach ($list as $item) {
            $no++;
            $row = array();
            $row[] = $no . ".";
            $row[] = $item->nama;
            $row[] = $item->keterangan;
            $row[] = '<img src="' . base_url("assets/uploads/" . $item->image) . '" height="30px"/>';
            if ($item->is_active == 0) {
                $row[] = '<center?><td align="center"><span class="label label-warning">Not Active</span></td></center>';
            } else if ($item->is_active == 1) {
                $row[] = '<center><td align="center"><span class="label label-success">Active</span></td></center>';
            }
            $row[] = '<center><button class="btn-link d-edit" onclick="edit_gallery_setting(' . "'" . $item->id . "'" . ')" title="Edit"><i class="fa fa-edit" style="color:green; font-size:12px;"></i></button><button class="btn-link d-delete" onclick="delete_gallery_setting(' . "'" . $item->id . "'" . ')" title="Hapus"><i class="fontello-trash-2" style="color:red; font-size:12px;"></i></button></center>';
            $data[] = $row;
        }
        $output = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->gallery->count_all(),
            "recordsFiltered" => $this->gallery->count_filtered(),
            "data" => $data,
        );
        // output to json format
        echo json_encode($output);
    }

    public function edit_data_gallery_setting($id)
    {
        is_logged_in();
        $data = $this->gallery->get_by_id($id);
        echo json_encode($data);
    }

    public function delete_gallery_settings($id)
    {
        $this->gallery->delete_gallery($id);
        echo 'Data submenu berhasil dihapus!';
    }

    public function update_gallery_settings()
    {
        $this->form_validation->set_rules('nama_gallery_edit', 'Nama Gallery', 'required|trim');

        $id = $this->input->post('gallery_id_edit');
        $where = ['id' => $id];
        //load date helper
        $this->load->helper('date');
        $format = "%Y-%m-%d %h:%i";

        if (!empty($_FILES["logo"]["name"])) {
            $this->_deleteImage($this->input->post('old_logo'));
            $logo = $this->_uploadImage("logo_gallery_");
        } else {
            $logo = $this->input->post('old_logo');
        }

        $data = [
            'nama' => $this->input->post('nama_gallery_edit'),
            'keterangan' => $this->input->post('ket_gallery_edit'),
            'image' => $logo,
            'is_active' => $this->input->post('gallery_status_id_edit')
        ];

        if ($this->form_validation->run() == true) {
            $this->gallery->edit_gallery($where, $data, 'cms_gallery_settings');
            $this->session->set_flashdata('message-gallery', '<div class="alert alert-success" role="alert">Perubahan data gallery sukses!</div>');
            redirect('admin/gallery_settings');
        } else {
            redirect('admin/gallery_settings');
        }
    }

    public function update_logo_header()
    {
        $this->form_validation->set_rules('content-header', 'Link', 'required|trim');
        $where = ['id' => "6"];
        //load date helper
        $this->load->helper('date');
        $format = "%Y-%m-%d %h:%i";

        if (!empty($_FILES["logo"]["name"])) {
            $this->_deleteImage($this->input->post('old_logo'));
            $logo = $this->_uploadImage("header_");
        } else {
            $logo = $this->input->post('old_logo');
        }

        $data = [
            'judul' => $this->input->post('title-header'),
            'content' => $this->input->post('content-header'),
            'image' => $logo,
            'updated_date' => @mdate($format)
        ];

        if ($this->form_validation->run() == true) {
            $this->tata_let->update($where, $data, 'cms_settings');
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Perubahan data logo header sukses!</div>');
            redirect('admin/tata_letak');
        } else {
            redirect('admin/tata_letak');
        }
    }

    public function update_menu()
    {
        $this->form_validation->set_rules('content-menu', 'Link', 'required|trim');
        $where = ['id' => "5"];
        //load date helper
        $this->load->helper('date');
        $format = "%Y-%m-%d %h:%i";

        $data = [
            'content' => $this->input->post('content-menu'),
            'updated_date' => @mdate($format)
        ];

        if ($this->form_validation->run() == true) {
            $this->tata_let->update($where, $data, 'cms_settings');
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Perubahan data menu header sukses!</div>');
            redirect('admin/tata_letak');
        } else {
            redirect('admin/tata_letak');
        }
    }

    public function update_column_1()
    {
        $this->form_validation->set_rules('content-column-1', 'Link', 'required|trim');
        $where = ['id' => "7"];
        //load date helper
        $this->load->helper('date');
        $format = "%Y-%m-%d %h:%i";

        $data = [
            'judul' => $this->input->post('title-column-1'),
            'content' => $this->input->post('content-column-1'),
            'updated_date' => @mdate($format)
        ];

        if ($this->form_validation->run() == true) {
            $this->tata_let->update($where, $data, 'cms_settings');
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Perubahan data footer 1 sukses!</div>');
            redirect('admin/tata_letak');
        } else {
            redirect('admin/tata_letak');
        }
    }

    public function update_column_2()
    {
        $this->form_validation->set_rules('content-column-2', 'Link', 'required|trim');
        $where = ['id' => "8"];
        //load date helper
        $this->load->helper('date');
        $format = "%Y-%m-%d %h:%i";

        $data = [
            'judul' => $this->input->post('title-column-2'),
            'content' => $this->input->post('content-column-2'),
            'updated_date' => @mdate($format)
        ];

        if ($this->form_validation->run() == true) {
            $this->tata_let->update($where, $data, 'cms_settings');
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Perubahan data footer 2 sukses!</div>');
            redirect('admin/tata_letak');
        } else {
            redirect('admin/tata_letak');
        }
    }

    public function update_column_3()
    {
        $this->form_validation->set_rules('content-column-3', 'Link', 'required|trim');
        $where = ['id' => "9"];
        //load date helper
        $this->load->helper('date');
        $format = "%Y-%m-%d %h:%i";

        $data = [
            'judul' => $this->input->post('title-column-3'),
            'content' => $this->input->post('content-column-3'),
            'updated_date' => @mdate($format)
        ];

        if ($this->form_validation->run() == true) {
            $this->tata_let->update($where, $data, 'cms_settings');
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Perubahan data footer 3 sukses!</div>');
            redirect('admin/tata_letak');
        } else {
            redirect('admin/tata_letak');
        }
    }

    public function update_column_4()
    {
        $this->form_validation->set_rules('content-column-4', 'Link', 'required|trim');
        $where = ['id' => "10"];
        //load date helper
        $this->load->helper('date');
        $format = "%Y-%m-%d %h:%i";

        $data = [
            'judul' => $this->input->post('title-column-4'),
            'content' => $this->input->post('content-column-4'),
            'updated_date' => @mdate($format)
        ];

        if ($this->form_validation->run() == true) {
            $this->tata_let->update($where, $data, 'cms_settings');
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Perubahan data footer 4 sukses!</div>');
            redirect('admin/tata_letak');
        } else {
            redirect('admin/tata_letak');
        }
    }

    public function update_footer_bawah()
    {
        $this->form_validation->set_rules('content-bawah', 'Content', 'required|trim');
        $where = ['id' => "4"];
        //load date helper
        $this->load->helper('date');

        $format = "%Y-%m-%d %h:%i";

        $data = [
            'content' => $this->input->post('content-bawah'),
            'updated_date' => @mdate($format)
        ];

        if ($this->form_validation->run() == true) {
            $this->tata_let->update($where, $data, 'cms_settings');
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Perubahan data footer sukses!</div>');
            redirect('admin/tata_letak');
        } else {
            redirect('admin/tata_letak');
        }
    }

    public function update_home_settings()
    {
        $this->form_validation->set_rules('content-home', 'Content', 'required|trim');
        $where = ['id' => "11"];
        //load date helper
        $this->load->helper('date');
        $format = "%Y-%m-%d %h:%i";

        if (!empty($_FILES["logo"]["name"])) {
            $this->_deleteImage($this->input->post('old_logo'));
            $logo = $this->_uploadImage("home_");
        } else {
            $logo = $this->input->post('old_logo');
        }

        $data = [
            'judul' => $this->input->post('title-home'),
            'content' => $this->input->post('content-home'),
            'image' => $logo,
            'updated_date' => @mdate($format)
        ];

        if ($this->form_validation->run() == true) {
            $this->tata_let->update($where, $data, 'cms_settings');
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Perubahan data home sukses!</div>');
            redirect('admin/home_settings');
        } else {
            redirect('admin/home_settings');
        }
    }

    public function update_about_settings()
    {
        $this->form_validation->set_rules('content-about', 'Content', 'required|trim');
        $where = ['id' => "12"];
        //load date helper
        $this->load->helper('date');
        $format = "%Y-%m-%d %h:%i";

        $data = [
            'judul' => $this->input->post('title-about'),
            'content' => $this->input->post('content-about'),
            'link' => $this->input->post('link-about'),
            'updated_date' => @mdate($format)
        ];

        if ($this->form_validation->run() == true) {
            $this->tata_let->update($where, $data, 'cms_settings');
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Perubahan data about sukses!</div>');
            redirect('admin/about_settings');
        } else {
            redirect('admin/about_settings');
        }
    }

    public function edit_data_team_setting($id)
    {
        is_logged_in();
        $data = $this->team->get_by_id($id);
        echo json_encode($data);
    }

    public function update_team_settings()
    {
        $this->form_validation->set_rules('nama_team_edit', 'Nama Team', 'required|trim');
        $this->form_validation->set_rules('jabatan_team_edit', 'Jabatan Team', 'required|trim');

        $id = $this->input->post('team_id_edit');
        $where = ['id' => $id];
        //load date helper
        $this->load->helper('date');
        $format = "%Y-%m-%d %h:%i";

        if (!empty($_FILES["logo"]["name"])) {
            $this->_deleteImage($this->input->post('old_logo'));
            $logo = $this->_uploadImage("logo_team_");
        } else {
            $logo = $this->input->post('old_logo');
        }

        $data = [
            'nama' => $this->input->post('nama_team_edit'),
            'jabatan' => $this->input->post('jabatan_team_edit'),
            'sosmed_twitter' => $this->input->post('sosmed_twitter_edit'),
            'sosmed_facebook' => $this->input->post('sosmed_facebook_edit'),
            'sosmed_instagram' => $this->input->post('sosmed_instagram_edit'),
            'sosmed_linkedin' => $this->input->post('sosmed_linkedin_edit'),
            'photo' => $logo,
            'is_active' => $this->input->post('team_status_id_edit')
        ];

        if ($this->form_validation->run() == true) {
            $this->team->edit_team($where, $data, 'cms_team_settings');
            $this->session->set_flashdata('message-client', '<div class="alert alert-success" role="alert">Perubahan data team sukses!</div>');
            redirect('admin/team_settings');
        } else {
            redirect('admin/team_settings');
        }
    }

    public function delete_team_settings($id)
    {
        $this->team->delete_team($id);
        echo 'Data submenu berhasil dihapus!';
    }

    private function _uploadImage($param)
    {
        date_default_timezone_set('Asia/Jakarta');
        $now = date('His');
        // Server
        // $config['upload_path'] = $_SERVER['DOCUMENT_ROOT'] . "/assets/uploads/";
        // Local
        $config['upload_path'] = $_SERVER['DOCUMENT_ROOT'] . "/oim/assets/uploads/";
        $config['file_name'] = "logo_" . $param . $now;
        $config['allowed_types'] = 'jpg|png|jpeg';

        $this->load->library('upload', $config);
        if (!$this->upload->do_upload("logo")) {
            $error = $this->upload->display_errors();
            // menampilkan pesan error
            print_r($error);
        } else {
            $upload = array('upload_data' => $this->upload->data());
            return $upload['upload_data']['file_name'];
        }

        return "default.png";
    }

    private function _deleteImage($file)
    {
        // Server
        // unlink($_SERVER['DOCUMENT_ROOT'] . "/assets/uploads/" . $file);
        // Local
        unlink($_SERVER['DOCUMENT_ROOT'] . "/oim/assets/uploads/" . $file);
    }
}
