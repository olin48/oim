<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Management extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('management_model');
        $this->load->library('session');
        is_logged_in();
    }

    public function users()
    {
        $this->form_validation->set_rules('first_name', 'Name', 'required|trim');
        $this->form_validation->set_rules('username', 'Username', 'required|trim');
        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');
        if ($this->form_validation->run() == false) {
            $this->load->model('Role_model', 'role');
            $config['web'] = $this->management_model->config_data()->result_array();
            $data['menu_title'] = "User Management";
            $data['user'] = $this->management_model->view_data()->result_array();
            $data['user'] = $this->role->getRoleName();
            $data['role'] = $this->management_model->get_role()->result_array();
            $data['url'] = "management/users";
            $this->load->view('templates/header', $config);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/breadcumb', $data);
            $this->load->view('management/users', $data);
            $this->load->view('templates/footer', $config);
            $this->load->view('templates/script_users');
        } else {
            $data = [
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'username' => $this->input->post('username'),
                'email' => $this->input->post('email'),
                'image' => 'default.jpg',
                'password' => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
                'role_id' => $this->input->post('role_id'),
                'id_kabkota' => $this->input->post('kabkota_id'),
                'id_korwil' => $this->input->post('korwil_id'),
                'is_active' => $this->input->post('status_id'),
                'date_created' => time()
            ];

            $this->db->insert('cms_user', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Penambahan data users baru sukses!</div>');
            redirect('management/users');
        }
    }

    public function edit_users($id)
    {
        $where = ['id' => $id];
        $pswd = $this->input->post('password');
        $data = null;
        if ($pswd != null) {
            $data = [
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'username' => $this->input->post('username'),
                'email' => $this->input->post('email'),
                'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
                'image' => 'default.jpg',
                'role_id' => $this->input->post('role_id'),
                'is_active' => $this->input->post('status_id'),
                'date_created' => time()
            ];
        } else {
            $data = [
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'username' => $this->input->post('username'),
                'email' => $this->input->post('email'),
                'image' => 'default.jpg',
                'role_id' => $this->input->post('role_id'),
                'is_active' => $this->input->post('status_id'),
                'date_created' => time()
            ];
        }

        $this->management_model->edit_users($where, $data, 'cms_user');
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Perubahan data users sukses!</div>');
        redirect('management/users');
    }

    public function delete_users($id)
    {
        $where = array('id' => $id);
        $this->management_model->delete_users($where, 'cms_user');
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Hapus data users sukses!</div>');
        redirect('management/users');
    }
}
