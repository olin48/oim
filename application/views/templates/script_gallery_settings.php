<script>
    (function($) {
        var dataMenuSetting = $('#dataGallerySetting').dataTable({
            "processing": true,
            "serverSide": true,
            ajax: {
                "url": "<?= site_url('admin/get_data_gallery') ?>",
                "type": "POST"
            },
            "columnDefs": [{
                    "targets": [0],
                    "className": 'text-center'
                },
                {
                    "targets": [0],
                    "orderable": false
                }
            ]
        });
    })(jQuery);

    function edit_gallery_setting(id) {
        // $('#form_edit_menu').get(0).reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#logo_photo').empty();
        $.ajax({
            url: "<?php echo site_url('admin/edit_data_gallery_setting') ?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                $('#gallery_id_edit').val(data.id);
                $('#nama_gallery_edit').val(data.nama);
                $('#ket_gallery_edit').val(data.keterangan);
                $('select[name="gallery_status_id_edit"]').val(data.is_active);
                $('#old_logo').val(data.image);
                $('#logo_photo').append("<img src='<?= base_url('assets/uploads/'); ?>" + data.image + "' height='150px'/>");
                if (data.is_active == 0) {
                    var status = "<td align='center'><span class='label label-warning'>Not Active</span></td>";
                } else if (data.is_active == 1) {
                    var status = "<td align='center'><span class='label label-success'>Active</span></td>";
                }
                $('#view_status').append(status);
                $('#editGallerySettings').modal('show'); // show bootstrap modal when complete loaded
                // $('.modal-title').text('Edit Menu'); // Set title to Bootstrap modal title
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error get data from ajax');
            }
        });
    }

    function delete_gallery_setting(id) {
        if (confirm('Apakah anda yakin ingin menghapus data ini?')) {
            $.ajax({
                type: "POST",
                url: "<?= site_url('admin/delete_gallery_settings/'); ?>" + id,
                data: {
                    id: id
                },
                success: function(data) {
                    $('#dataGallerySetting').DataTable().ajax.reload();
                }
            });
        } else {
            return false;
        }
    }
</script>

</Body>

</html>