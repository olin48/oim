<script>
    (function($) {
        var dataMenuSetting = $('#dataServiceSetting').dataTable({
            "processing": true,
            "serverSide": true,
            ajax: {
                "url": "<?= site_url('admin/get_data_service') ?>",
                "type": "POST"
            },
            "columnDefs": [{
                    "targets": [0],
                    "className": 'text-center'
                },
                {
                    "targets": [0],
                    "orderable": false
                }
            ]
        });
    })(jQuery);

    function edit_service_setting(id) {
        // $('#form_edit_menu').get(0).reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#logo_photo').empty();
        $.ajax({
            url: "<?php echo site_url('admin/edit_data_service_setting') ?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                $('#id_edit').val(data.id);
                $('#service_edit').val(data.service_name);
                $('#description_edit').val(data.service_desc);
                $('#url_edit').val(data.service_url);
                $('#old_logo').val(data.service_logo);
                $('select[name="service_id_edit"]').val(data.is_active);
                $('#logo_photo').append("<img src='<?= base_url('assets/uploads/'); ?>" + data.service_logo + "' height='80px'/>");
                $('#editServiceSettings').modal('show'); // show bootstrap modal when complete loaded
                // $('.modal-title').text('Edit Menu'); // Set title to Bootstrap modal title
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error get data from ajax');
            }
        });
    }

    function delete_service_setting(id) {
        if (confirm('Apakah anda yakin ingin menghapus data ini?')) {
            $.ajax({
                type: "POST",
                url: "<?= site_url('admin/delete_service_settings/'); ?>" + id,
                data: {
                    id: id
                },
                success: function(data) {
                    $('#dataServiceSetting').DataTable().ajax.reload();
                }
            });
        } else {
            return false;
        }
    }
</script>

</Body>

</html>