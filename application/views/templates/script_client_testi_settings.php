<script>
    (function($) {
        var dataMenuSetting = $('#dataClientTestiSetting').dataTable({
            "processing": true,
            "serverSide": true,
            ajax: {
                "url": "<?= site_url('admin/get_data_client_testi') ?>",
                "type": "POST"
            },
            "columnDefs": [{
                    "targets": [0],
                    "className": 'text-center'
                },
                {
                    "targets": [0],
                    "orderable": false
                }
            ]
        });
    })(jQuery);

    function edit_client_testi_setting(id) {
        // $('#form_edit_menu').get(0).reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#photo').empty();
        $.ajax({
            url: "<?php echo site_url('admin/edit_data_client_testi_setting') ?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                $('#client_testi_id_edit').val(data.id);
                $('#client_testi_edit').val(data.nama);
                $('#jabatan_testi_edit').val(data.jabatan);
                $('#komentar_testi_edit').val(data.komentar);
                $('select[name="client_testi_status_id_edit"]').val(data.is_active);
                $('#old_logo').val(data.photo);
                $('#photo').append("<img src='<?= base_url('assets/uploads/'); ?>" + data.photo + "' height='150px'/>");
                if (data.is_active == 0) {
                    var status = "<td align='center'><span class='label label-warning'>Not Active</span></td>";
                } else if (data.is_active == 1) {
                    var status = "<td align='center'><span class='label label-success'>Active</span></td>";
                }
                $('#view_status').append(status);
                $('#editClientTestiSettings').modal('show'); // show bootstrap modal when complete loaded
                // $('.modal-title').text('Edit Menu'); // Set title to Bootstrap modal title
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error get data from ajax');
            }
        });
    }

    function delete_client_testi_setting(id) {
        if (confirm('Apakah anda yakin ingin menghapus data ini?')) {
            $.ajax({
                type: "POST",
                url: "<?= site_url('admin/delete_client_testi_settings/'); ?>" + id,
                data: {
                    id: id
                },
                success: function(data) {
                    $('#dataClientTestiSetting').DataTable().ajax.reload();
                }
            });
        } else {
            return false;
        }
    }
</script>

</Body>

</html>