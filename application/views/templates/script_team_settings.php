<script>
    (function($) {
        var dataMenuSetting = $('#dataTeamSetting').dataTable({
            "processing": true,
            "serverSide": true,
            ajax: {
                "url": "<?= site_url('admin/get_data_team') ?>",
                "type": "POST"
            },
            "columnDefs": [{
                    "targets": [0],
                    "className": 'text-center'
                },
                {
                    "targets": [0],
                    "orderable": false
                }
            ]
        });
    })(jQuery);

    function edit_team_setting(id) {
        // $('#form_edit_menu').get(0).reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#logo_photo').empty();
        $.ajax({
            url: "<?php echo site_url('admin/edit_data_team_setting') ?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                $('#team_id_edit').val(data.id);
                $('#nama_team_edit').val(data.nama);
                $('#jabatan_team_edit').val(data.jabatan);
                $('#sosmed_twitter_edit').val(data.sosmed_twitter);
                $('#sosmed_facebook_edit').val(data.sosmed_facebook);
                $('#sosmed_instagram_edit').val(data.sosmed_instagram);
                $('#sosmed_linkedin_edit').val(data.sosmed_linkedin);
                $('select[name="team_status_id_edit"]').val(data.is_active);
                $('#old_logo').val(data.photo);
                $('#logo_photo').append("<img src='<?= base_url('assets/uploads/'); ?>" + data.photo + "' height='150px'/>");
                if (data.is_active == 0) {
                    var status = "<td align='center'><span class='label label-warning'>Not Active</span></td>";
                } else if (data.is_active == 1) {
                    var status = "<td align='center'><span class='label label-success'>Active</span></td>";
                }
                $('#view_status').append(status);
                $('#editTeamSettings').modal('show'); // show bootstrap modal when complete loaded
                // $('.modal-title').text('Edit Menu'); // Set title to Bootstrap modal title
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error get data from ajax');
            }
        });
    }

    function delete_team_setting(id) {
        if (confirm('Apakah anda yakin ingin menghapus data ini?')) {
            $.ajax({
                type: "POST",
                url: "<?= site_url('admin/delete_team_settings/'); ?>" + id,
                data: {
                    id: id
                },
                success: function(data) {
                    $('#dataTeamSetting').DataTable().ajax.reload();
                }
            });
        } else {
            return false;
        }
    }
</script>

</Body>

</html>