<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title><?= $config->name; ?></title>
    <meta content="" name="descriptison">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="<?= base_url('assets/front/img/favicon.png'); ?>" rel="icon">
    <link href="<?= base_url('assets/front/img/apple-touch-icon.png'); ?>" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Dosis:300,400,500,,600,700,700i|Lato:300,300i,400,400i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="<?= base_url('assets/front/vendor/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?= base_url('assets/front/vendor/icofont/icofont.min.css'); ?>" rel="stylesheet">
    <link href="<?= base_url('assets/front/vendor/boxicons/css/boxicons.min.css'); ?>" rel="stylesheet">
    <link href="<?= base_url('assets/front/vendor/venobox/venobox.css'); ?>" rel="stylesheet">
    <link href="<?= base_url('assets/front/vendor/line-awesome/css/line-awesome.min.css'); ?>" rel="stylesheet">
    <link href="<?= base_url('assets/front/vendor/owl.carousel/assets/owl.carousel.min.css'); ?>" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="<?= base_url('assets/front/css/style.css'); ?>" rel="stylesheet">

    <!-- =======================================================
    * Template Name: Butterfly - v2.1.0
    * Template URL: https://bootstrapmade.com/butterfly-free-bootstrap-theme/
    * Author: BootstrapMade.com
    * License: https://bootstrapmade.com/license/
    ======================================================== -->
</head>

<body>
    <!-- ======= Header ======= -->
    <header id="header" class="fixed-top">
        <div class="container d-flex align-items-center">

            <a href="<?= $logo->content; ?>" class="logo mr-auto"><img src="<?= base_url('assets/uploads/'); ?><?= $logo->image; ?>" alt="" class="img-fluid"></a>
            <!-- Uncomment below if you prefer to use text as a logo -->
            <!-- <h1 class="logo mr-auto"><a href="index.html">Butterfly</a></h1> -->

            <?= $menu->content; ?>

        </div>
    </header><!-- End Header -->