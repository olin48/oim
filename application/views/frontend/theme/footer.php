<!-- ======= Footer ======= -->
<footer id="footer">

    <div class="footer-newsletter">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <h4>Join Our Newsletter</h4>
                    <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>
                    <form action="" method="post">
                        <input type="email" name="email"><input type="submit" value="Subscribe">
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="footer-top">
        <div class="container">
            <div class="row">

                <div class="col-lg-3 col-md-6 footer-contact">
                    <h3><?= $footer1->judul; ?></h3>
                    <?= $footer1->content; ?>
                </div>

                <div class="col-lg-3 col-md-6 footer-links">
                    <h4><?= $footer2->judul; ?></h4>
                    <?= $footer2->content; ?>
                </div>

                <div class="col-lg-3 col-md-6 footer-links">
                    <h4><?= $footer3->judul; ?></h4>
                    <?= $footer3->content; ?>
                </div>

                <div class="col-lg-3 col-md-6 footer-links">
                    <h4><?= $footer4->judul; ?></h4>
                    <?= $footer4->content; ?>
                </div>

            </div>
        </div>
    </div>

    <div class="container py-4">
        <?= $footer->content; ?>
    </div>
</footer><!-- End Footer -->

<a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

<!-- Vendor JS Files -->
<script src="<?= base_url('assets/front/vendor/jquery/jquery.min.js'); ?>"></script>
<script src="<?= base_url('assets/front/vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
<script src="<?= base_url('assets/front/vendor/jquery.easing/jquery.easing.min.js'); ?>"></script>
<script src="<?= base_url('assets/front/vendor/php-email-form/validate.js'); ?>"></script>
<script src="<?= base_url('assets/front/vendor/venobox/venobox.min.js'); ?>"></script>
<script src="<?= base_url('assets/front/vendor/waypoints/jquery.waypoints.min.js'); ?>"></script>
<script src="<?= base_url('assets/front/vendor/counterup/counterup.min.js'); ?>"></script>
<script src="<?= base_url('assets/front/vendor/isotope-layout/isotope.pkgd.min.js'); ?>"></script>
<script src="<?= base_url('assets/front/vendor/owl.carousel/owl.carousel.min.js'); ?>"></script>

<!-- Template Main JS File -->
<script src="<?= base_url('assets/front/js/main.js'); ?>"></script>

</body>

</html>