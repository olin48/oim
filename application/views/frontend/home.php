<!-- ======= Hero Section ======= -->
<section id="hero" class="d-flex align-items-center">

	<div class="container">
		<div class="row">
			<div class="col-lg-6 pt-4 pt-lg-0 order-2 order-lg-1 d-flex flex-column justify-content-center">
				<h1><?= $home->judul; ?></h1>
				<h2><?= $home->content; ?></h2>
				<div><a href="#about" class="btn-get-started scrollto">Selengkapnya</a></div>
			</div>
			<div class="col-lg-6 order-1 order-lg-2 hero-img">
				<img src="<?= base_url('assets/uploads/'); ?><?= $home->image; ?>" class="img-fluid" alt="">
			</div>
		</div>
	</div>

</section><!-- End Hero -->

<main id="main">

	<!-- ======= About Section ======= -->
	<section id="about" class="about">
		<div class="container">

			<div class="row">
				<div class="col-xl-5 col-lg-6 d-flex justify-content-center video-box align-items-stretch">
					<a href="<?= $about->link; ?>" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true"></a>
				</div>

				<div class="col-xl-7 col-lg-6 icon-boxes d-flex flex-column align-items-stretch justify-content-center py-5 px-lg-5">
					<h3><?= $about->judul; ?></h3>
					<?= $about->content; ?>

				</div>
			</div>

		</div>
	</section><!-- End About Section -->

	<!-- ======= Counts Section ======= -->
	<section id="counts" class="counts">
		<div class="container">

			<div class="text-center title">
				<h3>What we have achieved so far</h3>
				<p>Iusto et labore modi qui sapiente xpedita tempora et aut non ipsum consequatur illo.</p>
			</div>

			<div class="row counters">

				<div class="col-lg-3 col-6 text-center">
					<span data-toggle="counter-up">232</span>
					<p>Clients</p>
				</div>

				<div class="col-lg-3 col-6 text-center">
					<span data-toggle="counter-up">521</span>
					<p>Projects</p>
				</div>

				<div class="col-lg-3 col-6 text-center">
					<span data-toggle="counter-up">1,463</span>
					<p>Hours Of Support</p>
				</div>

				<div class="col-lg-3 col-6 text-center">
					<span data-toggle="counter-up">15</span>
					<p>Hard Workers</p>
				</div>

			</div>

		</div>
	</section><!-- End Counts Section -->

	<!-- ======= Kerjasama Section ======= -->
	<section id="clients" class="clients">
		<div class="container">
			<div class="section-title">
				<h2>Kerjasama Kami</h2>
			</div>
			<div class="row no-gutters clients-wrap clearfix wow fadeInUp">
				<?php foreach ($kerjasama as $kc) : ?>
					<div class="col-lg-3 col-md-4 col-6">
						<div class="client-logo">
							<img src="<?= base_url('assets/uploads/'); ?><?= $kc['image']; ?>" class="img-fluid" alt="">
						</div>
					</div>
				<?php endforeach; ?>
			</div>

		</div>
	</section><!-- End Clients Section -->
	<!-- ======= Client Section ======= -->
	<section id="clients" class="clients">
		<div class="container">
			<div class="section-title">
				<h2>Klien Kami</h2>
			</div>
			<div class="row no-gutters clients-wrap clearfix wow fadeInUp">
				<?php foreach ($client as $c) : ?>
					<div class="col-lg-3 col-md-4 col-6">
						<div class="client-logo">
							<img src="<?= base_url('assets/uploads/'); ?><?= $c['image']; ?>" class="img-fluid" alt="">
						</div>
					</div>
				<?php endforeach; ?>
			</div>

		</div>
	</section><!-- End Clients Section -->

	<!-- ======= Services Section ======= -->
	<section id="services" class="services section-bg">
		<div class="container">

			<div class="section-title">
				<h2><?= $service_settings->judul; ?></h2>
				<p><?= $service_settings->content; ?></p>
			</div>

			<div class="row">
				<?php foreach ($service as $s) : ?>
					<div class="col-lg-4 col-md-6">
						<div class="icon-box">
							<div class="icom" style="text-align: center;"><img src="<?= base_url('assets/uploads/'); ?><?= $s['service_logo']; ?>" height="75px"></div>
							<br />
							<h4 class="title"><a href="<?= $s['service_url']; ?>" target="_blank"><?= $s['service_name']; ?></a></h4>
							<p class="description"><?= $s['service_desc']; ?></p>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</section><!-- End Services Section -->

	<!-- ======= Portfolio Section ======= -->
	<!-- <section id="portfolio" class="portfolio">
		<div class="container">

			<div class="section-title">
				<h2>Portfolio</h2>
				<p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
			</div>

			<div class="row">
				<div class="col-lg-12 d-flex justify-content-center">
					<ul id="portfolio-flters">
						<li data-filter="*" class="filter-active">All</li>
						<li data-filter=".filter-app">App</li>
						<li data-filter=".filter-card">Card</li>
						<li data-filter=".filter-web">Web</li>
					</ul>
				</div>
			</div>

			<div class="row portfolio-container">

				<div class="col-lg-4 col-md-6 portfolio-item filter-app">
					<div class="portfolio-wrap">
						<img src="<?= base_url('assets/front/img/portfolio/portfolio-1.jpg'); ?>" class="img-fluid" alt="">
						<div class="portfolio-info">
							<h4>App 1</h4>
							<p>App</p>
							<div class="portfolio-links">
								<a href="<?= base_url('assets/front/img/portfolio/portfolio-1.jpg'); ?>" data-gall="portfolioGallery" class="venobox" title="App 1"><i class="bx bx-plus"></i></a>
								<a href="portfolio-details.html" title="More Details"><i class="bx bx-link"></i></a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-4 col-md-6 portfolio-item filter-web">
					<div class="portfolio-wrap">
						<img src="<?= base_url('assets/front/img/portfolio/portfolio-2.jpg'); ?>" class="img-fluid" alt="">
						<div class="portfolio-info">
							<h4>Web 3</h4>
							<p>Web</p>
							<div class="portfolio-links">
								<a href="<?= base_url('assets/front/img/portfolio/portfolio-2.jpg'); ?>" data-gall="portfolioGallery" class="venobox" title="Web 3"><i class="bx bx-plus"></i></a>
								<a href="portfolio-details.html" title="More Details"><i class="bx bx-link"></i></a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-4 col-md-6 portfolio-item filter-app">
					<div class="portfolio-wrap">
						<img src="<?= base_url('assets/front/img/portfolio/portfolio-3.jpg'); ?>" class="img-fluid" alt="">
						<div class="portfolio-info">
							<h4>App 2</h4>
							<p>App</p>
							<div class="portfolio-links">
								<a href="<?= base_url('assets/front/img/portfolio/portfolio-3.jpg'); ?>" data-gall="portfolioGallery" class="venobox" title="App 2"><i class="bx bx-plus"></i></a>
								<a href="portfolio-details.html" title="More Details"><i class="bx bx-link"></i></a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-4 col-md-6 portfolio-item filter-card">
					<div class="portfolio-wrap">
						<img src="<?= base_url('assets/front/img/portfolio/portfolio-4.jpg'); ?>" class="img-fluid" alt="">
						<div class="portfolio-info">
							<h4>Card 2</h4>
							<p>Card</p>
							<div class="portfolio-links">
								<a href="<?= base_url('assets/front/img/portfolio/portfolio-4.jpg'); ?>" data-gall="portfolioGallery" class="venobox" title="Card 2"><i class="bx bx-plus"></i></a>
								<a href="portfolio-details.html" title="More Details"><i class="bx bx-link"></i></a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-4 col-md-6 portfolio-item filter-web">
					<div class="portfolio-wrap">
						<img src="<?= base_url('assets/front/img/portfolio/portfolio-5.jpg'); ?>" class="img-fluid" alt="">
						<div class="portfolio-info">
							<h4>Web 2</h4>
							<p>Web</p>
							<div class="portfolio-links">
								<a href="<?= base_url('assets/front/img/portfolio/portfolio-5.jpg'); ?>" data-gall="portfolioGallery" class="venobox" title="Web 2"><i class="bx bx-plus"></i></a>
								<a href="portfolio-details.html" title="More Details"><i class="bx bx-link"></i></a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-4 col-md-6 portfolio-item filter-app">
					<div class="portfolio-wrap">
						<img src="<?= base_url('assets/front/img/portfolio/portfolio-6.jpg'); ?>" class="img-fluid" alt="">
						<div class="portfolio-info">
							<h4>App 3</h4>
							<p>App</p>
							<div class="portfolio-links">
								<a href="<?= base_url('assets/front/img/portfolio/portfolio-6.jpg'); ?>" data-gall="portfolioGallery" class="venobox" title="App 3"><i class="bx bx-plus"></i></a>
								<a href="portfolio-details.html" title="More Details"><i class="bx bx-link"></i></a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-4 col-md-6 portfolio-item filter-card">
					<div class="portfolio-wrap">
						<img src="<?= base_url('assets/front/img/portfolio/portfolio-7.jpg'); ?>" class="img-fluid" alt="">
						<div class="portfolio-info">
							<h4>Card 1</h4>
							<p>Card</p>
							<div class="portfolio-links">
								<a href="<?= base_url('assets/front/img/portfolio/portfolio-7.jpg'); ?>" data-gall="portfolioGallery" class="venobox" title="Card 1"><i class="bx bx-plus"></i></a>
								<a href="portfolio-details.html" title="More Details"><i class="bx bx-link"></i></a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-4 col-md-6 portfolio-item filter-card">
					<div class="portfolio-wrap">
						<img src="<?= base_url('assets/front/img/portfolio/portfolio-8.jpg'); ?>" class="img-fluid" alt="">
						<div class="portfolio-info">
							<h4>Card 3</h4>
							<p>Card</p>
							<div class="portfolio-links">
								<a href="<?= base_url('assets/front/img/portfolio/portfolio-8.jpg'); ?>" data-gall="portfolioGallery" class="venobox" title="Card 3"><i class="bx bx-plus"></i></a>
								<a href="portfolio-details.html" title="More Details"><i class="bx bx-link"></i></a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-4 col-md-6 portfolio-item filter-web">
					<div class="portfolio-wrap">
						<img src="<?= base_url('assets/front/img/portfolio/portfolio-9.jpg'); ?>" class="img-fluid" alt="">
						<div class="portfolio-info">
							<h4>Web 3</h4>
							<p>Web</p>
							<div class="portfolio-links">
								<a href="<?= base_url('assets/front/img/portfolio/portfolio-9.jpg'); ?>" data-gall="portfolioGallery" class="venobox" title="Web 3"><i class="bx bx-plus"></i></a>
								<a href="portfolio-details.html" title="More Details"><i class="bx bx-link"></i></a>
							</div>
						</div>
					</div>
				</div>

			</div>

		</div>
	</section> -->
	<!-- End Portfolio Section -->

	<!-- ======= Testimonials Section ======= -->
	<section id="testimonials" class="testimonials">
		<div class="container">
			<div class="owl-carousel testimonials-carousel">
				<?php foreach ($client_testi as $ct) : ?>
					<div class="testimonial-item">
						<img src="<?= base_url('assets/uploads/'); ?><?= $ct['photo']; ?>" class="testimonial-img" alt="">
						<h3><?= $ct['nama']; ?></h3>
						<h4><?= $ct['jabatan']; ?></h4>
						<p>
							<i class="bx bxs-quote-alt-left quote-icon-left"></i>
							<?= $ct['komentar']; ?>
							<i class="bx bxs-quote-alt-right quote-icon-right"></i>
						</p>
					</div>
				<?php endforeach; ?>
			</div>

		</div>
	</section><!-- End Testimonials Section -->

	<!-- ======= Team Section ======= -->
	<section id="team" class="team section-bg">
		<div class="container">

			<div class="section-title">
				<h2><?= $team_settings->judul; ?></h2>
				<p><?= $team_settings->content; ?></p>
			</div>

			<div class="row">
				<?php foreach ($team as $t) : ?>
					<div class="col-lg-3 col-md-6 d-flex align-items-stretch">
						<div class="member">
							<div class="member-img">
								<img src="<?= base_url('assets/uploads/'); ?><?= $t['photo']; ?>" class="img-fluid" alt="">
								<div class="social">
									<a href="<?= $t['sosmed_twitter']; ?>" target="_blank"><i class="icofont-twitter"></i></a>
									<a href="<?= $t['sosmed_facebook']; ?>" target="_blank"><i class="icofont-facebook"></i></a>
									<a href="<?= $t['sosmed_instagram']; ?>" target="_blank"><i class="icofont-instagram"></i></a>
									<a href="<?= $t['sosmed_linkedin']; ?>" target="_blank"><i class="icofont-linkedin"></i></a>
								</div>
							</div>
							<div class="member-info">
								<h4><?= $t['nama']; ?></h4>
								<span><?= $t['jabatan']; ?></span>
							</div>
						</div>
					</div>

				<?php endforeach; ?>
			</div>

		</div>
	</section><!-- End Team Section -->

	<!-- ======= Gallery Section ======= -->
	<section id="gallery" class="gallery">
		<div class="container">

			<div class="section-title">
				<h2><?= $gallery_settings->judul; ?></h2>
				<p><?= $gallery_settings->content; ?></p>
			</div>

			<div class="row no-gutters">
				<?php foreach ($gallery as $g) : ?>
					<div class="col-lg-3 col-md-4">
						<div class="gallery-item">
							<a href="<?= base_url('assets/uploads/'); ?><?= $g['image']; ?>" class="venobox" data-gall="gallery-item">
								<img src="<?= base_url('assets/uploads/'); ?><?= $g['image']; ?>" alt="" class="img-fluid">
							</a>
						</div>
					</div>
				<?php endforeach; ?>
			</div>

		</div>
	</section><!-- End Gallery Section -->

	<!-- ======= Contact Section ======= -->
	<section id="contact" class="contact">
		<div class="container">

			<div class="section-title">
				<h2>Contact</h2>
				<p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
			</div>

			<div>
				<iframe style="border:0; width: 100%; height: 270px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3965.790118456067!2d106.98362611323005!3d-6.291292895446612!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e698dd23b9dd041%3A0xb7c4922f0aba6225!2sPT%20OTRISMO%20INDUSTRI%20MANDIRI!5e0!3m2!1sen!2sid!4v1600537170966!5m2!1sen!2sid" frameborder="0" allowfullscreen></iframe>
			</div>

			<div class="row mt-5">

				<div class="col-lg-4">
					<div class="info">
						<div class="address">
							<i class="icofont-google-map"></i>
							<h4>Location:</h4>
							<p>Jalan Gardenia Raya, Rt. 005/Rw. 002, Bojong Rawalumbu. Kecamatan Rawalumbu, Kota Bekasi, Jawa Barat, <br /><b>Kode Pos</b> : 17116</p>
						</div>

						<div class="email">
							<i class="icofont-envelope"></i>
							<h4>Email:</h4>
							<p>admin@otrismoindustrimandiri.com</p>
						</div>

						<div class="phone">
							<i class="icofont-phone"></i>
							<h4>Call:</h4>
							<p>(021) 82764905</p>
						</div>

					</div>

				</div>

				<div class="col-lg-8 mt-5 mt-lg-0">

					<form action="forms/contact.php" method="post" role="form" class="php-email-form">
						<div class="form-row">
							<div class="col-md-6 form-group">
								<input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
								<div class="validate"></div>
							</div>
							<div class="col-md-6 form-group">
								<input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
								<div class="validate"></div>
							</div>
						</div>
						<div class="form-group">
							<input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
							<div class="validate"></div>
						</div>
						<div class="form-group">
							<textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
							<div class="validate"></div>
						</div>
						<div class="mb-3">
							<div class="loading">Loading</div>
							<div class="error-message"></div>
							<div class="sent-message">Your message has been sent. Thank you!</div>
						</div>
						<div class="text-center"><button type="submit">Send Message</button></div>
					</form>

				</div>

			</div>

		</div>
	</section><!-- End Contact Section -->

</main><!-- End #main -->