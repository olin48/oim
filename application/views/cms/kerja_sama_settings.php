<!-- CONTENT -->
<div class="wrap-fluid" id="paper-bg">
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="news-widget">
                        <h2>
                            <span class="bg-red"><?= $menu_title; ?></span>
                        </h2>
                    </div>
                    <div class="box-body table-responsive">
                        <?= $this->session->flashdata('message-kerjasama'); ?>
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#kerjasamaSetting">Tambah Kerja Sama</button>
                        <br /><br />
                        <table id="dataKerjasamaSetting" class="table table-bordered table-striped" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th style="width: 5%;">#</th>
                                    <th>Nama</th>
                                    <th>Link URL</th>
                                    <th style="width: 18%;">Logo</th>
                                    <th style="width: 10%;">Status</th>
                                    <th style="width: 15%;">Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>

        <div id="kerjasamaSetting" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Tambah Kerja Sama</h4>
                    </div>

                    <?php echo form_open_multipart('admin/add_kerjasama_setting'); ?>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="usr">Nama :</label>
                            <input type="text" class="form-control" id="nama_kerjasama_add" name="nama_kerjasama_add">
                        </div>
                        <div class="form-group">
                            <label for="usr">Link URL :</label>
                            <input type="text" class="form-control" id="link_url_kerjasama_add" name="link_url_kerjasama_add">
                        </div>
                        <div class="form-group">
                            <label for="usr">Logo :</label>
                            <input type="file" class="form-control-file" id="logo" name="logo"></input>
                            <i>Maksimal file upload 2MB, dengan format .jpg, .png dan .jpeg</i></br>
                        </div>
                        <div class="form-group">
                            <label for="usr">Status :</label>
                            <select name="kerjasama_status_id_add" id="kerjasama_status_id_add" class="form-control" required>
                                <option value="">Pilih Status</option>
                                <option value="0">Non Active</option>
                                <option value="1">Active</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-success">Simpan</button>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>

        <div id="editKerjasamaSettings" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit Kerja Sama</h4>
                    </div>

                    <?php echo form_open_multipart('admin/update_kerjasama_settings'); ?>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="usr">Nama :</label>
                            <input type="hidden" class="form-control" id="kerjasama_id_edit" name="kerjasama_id_edit">
                            <input type="text" class="form-control" id="nama_kerjasama_edit" name="nama_kerjasama_edit">
                        </div>
                        <div class="form-group">
                            <label for="usr">Link URL :</label>
                            <input type="text" class="form-control" id="link_url_kerjasama_edit" name="link_url_kerjasama_edit">
                        </div>
                        <div class="form-group">
                            <label for="usr">Logo :</label>
                            <input type="hidden" class="form-control" id="old_logo" name="old_logo">
                            <input type="file" class="form-control-file" id="logo" name="logo"></input>
                            <i>Maksimal file upload 2MB, dengan format .jpg, .png dan .jpeg</i></br>
                            <br />
                            <span id="logo_image" name="logo_image">
                        </div>
                        <div class="form-group">
                            <label for="usr">Status :</label>
                            <select name="kerjasama_status_id_edit" id="kerjasama_status_id_edit" class="form-control" required>
                                <option value="">Pilih Status</option>
                                <option value="0">Non Active</option>
                                <option value="1">Active</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-success">Simpan</button>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- #/paper bg -->
</div>
<!-- ./wrap-sidebar-content -->

<!-- / END OF CONTENT -->