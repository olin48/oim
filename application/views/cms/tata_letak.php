<!-- CONTENT -->
<div class="wrap-fluid" id="paper-bg">
    <div class="row">
        <div class="col-lg-4">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="news-widget">
                        <h2>
                            <span class="bg-green">Header</span>
                        </h2>
                    </div>
                    <?php foreach ($logo as $l) : ?>
                        <span style="font-size: 15px; margin-left: 10px;"><?= $l['judul']; ?> (Header)</span><br />
                        <span style="font-size: 13px; margin-left: 10px;">Gadget Header Halaman</span><br />
                        <a href="#" data-toggle="modal" data-target="#edit_logo_header">
                            <p style="font-size: 12px; text-align:right; margin-right: 10px; color:blue;">Edit</p>
                        </a>
                    <?php endforeach; ?>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>

        <div class="col-lg-8">
            <div class="box">
                <div class="box-body">
                    <div class="news-widget">
                        <h2>
                            <span class="bg-green">Menu Header</span>
                        </h2>
                    </div>
                    <span style="font-size: 15px; margin-left: 10px;">HTML/JavaScript (Menu)</span><br />
                    <span style="font-size: 13px; margin-left: 10px;">Gadget HTML/JavaScript</span><br />
                    <a href="#" data-toggle="modal" data-target="#edit_menu">
                        <p style="font-size: 12px; text-align:right; margin-right: 10px; color:blue;">Edit</p>
                    </a>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>

    <div class="row">
        <div class="col-lg-3">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="news-widget">
                        <h2>
                            <span class="bg-aqua">Footer 1</span>
                        </h2>
                    </div>
                    <span style="font-size: 15px; margin-left: 10px;">HTML/JavaScript</span><br />
                    <span style="font-size: 13px; margin-left: 10px;">Gadget HTML/JavaScript</span><br />
                    <a href="#" data-toggle="modal" data-target="#edit_column_1">
                        <p style="font-size: 12px; text-align:right; margin-right: 10px; color:blue;">Edit</p>
                    </a>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>

        <div class="col-lg-3">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="news-widget">
                        <h2>
                            <span class="bg-aqua">Footer 2</span>
                        </h2>
                    </div>
                    <span style="font-size: 15px; margin-left: 10px;">HTML/JavaScript</span><br />
                    <span style="font-size: 13px; margin-left: 10px;">Gadget HTML/JavaScript</span><br />
                    <a href="#" data-toggle="modal" data-target="#edit_column_2">
                        <p style="font-size: 12px; text-align:right; margin-right: 10px; color:blue;">Edit</p>
                    </a>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>

        <div class="col-lg-3">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="news-widget">
                        <h2>
                            <span class="bg-aqua">Footer 3</span>
                        </h2>
                    </div>
                    <span style="font-size: 15px; margin-left: 10px;">HTML/JavaScript</span><br />
                    <span style="font-size: 13px; margin-left: 10px;">Gadget HTML/JavaScript</span><br />
                    <a href="#" data-toggle="modal" data-target="#edit_column_3">
                        <p style="font-size: 12px; text-align:right; margin-right: 10px; color:blue;">Edit</p>
                    </a>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>

        <div class="col-lg-3">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="news-widget">
                        <h2>
                            <span class="bg-aqua">Footer 4</span>
                        </h2>
                    </div>
                    <span style="font-size: 15px; margin-left: 10px;">HTML/JavaScript</span><br />
                    <span style="font-size: 13px; margin-left: 10px;">Gadget HTML/JavaScript</span><br />
                    <a href="#" data-toggle="modal" data-target="#edit_column_4">
                        <p style="font-size: 12px; text-align:right; margin-right: 10px; color:blue;">Edit</p>
                    </a>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>

        <div class="col-lg-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="news-widget">
                        <h2>
                            <span class="bg-red">Footer</span>
                        </h2>
                    </div>
                    <span style="font-size: 15px; margin-left: 10px;">HTML/JavaScript (Footer)</span><br />
                    <span style="font-size: 13px; margin-left: 10px;">Gadget HTML/JavaScript</span><br />
                    <a href="#" data-toggle="modal" data-target="#edit_footer">
                        <p style="font-size: 12px; text-align:right; margin-right: 10px; color:blue;">Edit</p>
                    </a>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

    <!-- Modal Footer -->
    <div id="edit_logo_header" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Logo Settings</h4>
                </div>

                <?php echo form_open_multipart('admin/update_logo_header'); ?>
                <div class="modal-body">
                    <?php foreach ($logo as $l) : ?>
                        <div class="form-group">
                            <label for="usr">Title :</label>
                            <input type="text" class="form-control" id="title-header" name="title-header" value="<?= $l['judul']; ?>">
                        </div>
                        <div class="form-group">
                            <label for="usr">Link :</label>
                            <input type="text" class="form-control" id="content-header" name="content-header" value="<?= $l['content']; ?>">
                        </div>
                        <div class="form-group">
                            <label for="usr">Logo :</label>
                            <input type="file" class="form-control-file" id="logo" name="logo"></input>
                            <input type="hidden" id="old_logo" name="old_logo" value="<?= $l['image']; ?>" />
                            <i>Maksimal file upload 2MB, dengan format .jpg, .png dan .jpeg</i></br>
                            <img src="<?php echo base_url('assets/uploads/'); ?><?= $l['image']; ?>" height="100px" style="margin-top: 10px;">
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success">Simpan</button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>

    <div id="edit_menu" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Custom HTML</h4>
                </div>

                <?php echo form_open_multipart('admin/update_menu'); ?>
                <div class="modal-body">
                    <?php foreach ($menu_header as $mh) : ?>
                        <div class="form-group">
                            <label for="usr">Custom Menu :</label>
                            <textarea class="form-control" id="content-menu" name="content-menu" style="height: 250px"><?= $mh['content']; ?></textarea>
                        </div>

                    <?php endforeach; ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success">Simpan</button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>

    <div id="edit_column_1" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Footer Column 1</h4>
                </div>

                <?php echo form_open_multipart('admin/update_column_1'); ?>
                <div class="modal-body">
                    <?php foreach ($column_1 as $c1) : ?>
                        <div class="form-group">
                            <label for="usr">Title :</label>
                            <input type="text" class="form-control" id="title-column-1" name="title-column-1" value="<?= $c1['judul']; ?>">
                        </div>
                        <div class="form-group">
                            <label for="usr">Content :</label>
                            <textarea class="form-control" id="content-column-1" name="content-column-1" style="height: 250px"><?= $c1['content']; ?></textarea>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success">Simpan</button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>

    <div id="edit_column_2" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Footer Column 2</h4>
                </div>

                <?php echo form_open_multipart('admin/update_column_2'); ?>
                <div class="modal-body">
                    <?php foreach ($column_2 as $c2) : ?>
                        <div class="form-group">
                            <label for="usr">Title :</label>
                            <input type="text" class="form-control" id="title-column-2" name="title-column-2" value="<?= $c2['judul']; ?>">
                        </div>
                        <div class="form-group">
                            <label for="usr">Content :</label>
                            <textarea class="form-control" id="content-column-2" name="content-column-2" style="height: 250px"><?= $c2['content']; ?></textarea>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success">Simpan</button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>

    <div id="edit_column_3" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Footer Column 3</h4>
                </div>

                <?php echo form_open_multipart('admin/update_column_3'); ?>
                <div class="modal-body">
                    <?php foreach ($column_3 as $c3) : ?>
                        <div class="form-group">
                            <label for="usr">Title :</label>
                            <input type="text" class="form-control" id="title-column-3" name="title-column-3" value="<?= $c3['judul']; ?>">
                        </div>
                        <div class="form-group">
                            <label for="usr">Content :</label>
                            <textarea class="form-control" id="content-column-3" name="content-column-3" style="height: 250px"><?= $c3['content']; ?></textarea>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success">Simpan</button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>

    <div id="edit_column_4" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Footer Column 4</h4>
                </div>

                <?php echo form_open_multipart('admin/update_column_4'); ?>
                <div class="modal-body">
                    <?php foreach ($column_4 as $c4) : ?>
                        <div class="form-group">
                            <label for="usr">Title :</label>
                            <input type="text" class="form-control" id="title-column-4" name="title-column-4" value="<?= $c4['judul']; ?>">
                        </div>
                        <div class="form-group">
                            <label for="usr">Content :</label>
                            <textarea class="form-control" id="content-column-4" name="content-column-4" style="height: 250px"><?= $c4['content']; ?></textarea>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success">Simpan</button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>

    <div id="edit_footer" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Custom HTML</h4>
                </div>

                <?php echo form_open_multipart('admin/update_footer_bawah'); ?>
                <div class="modal-body">
                    <?php foreach ($footer as $f) : ?>
                        <div class="form-group">
                            <label for="usr">Copyright Text :</label>
                            <textarea class="form-control" id="content-bawah" name="content-bawah" style="height: 250px"><?= $f['content']; ?></textarea>
                        </div>

                    <?php endforeach; ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success">Simpan</button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>

</div>
<!-- #/paper bg -->
</div>
<!-- ./wrap-sidebar-content -->

<!-- / END OF CONTENT -->