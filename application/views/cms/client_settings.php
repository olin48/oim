<!-- CONTENT -->
<div class="wrap-fluid" id="paper-bg">
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="news-widget">
                        <h2>
                            <span class="bg-red"><?= $menu_title; ?></span>
                        </h2>
                    </div>
                    <div class="box-body table-responsive">
                        <?= $this->session->flashdata('message-client'); ?>
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#clientSetting">Tambah Client</button>
                        <br /><br />
                        <table id="dataClientSetting" class="table table-bordered table-striped" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th style="width: 5%;">#</th>
                                    <th>Nama Client</th>
                                    <th>Link URL</th>
                                    <th style="width: 18%;">Logo</th>
                                    <th style="width: 10%;">Status</th>
                                    <th style="width: 15%;">Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>

        <div id="clientSetting" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Tambah Client</h4>
                    </div>

                    <?php echo form_open_multipart('admin/add_client_setting'); ?>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="usr">Nama Client :</label>
                            <input type="text" class="form-control" id="nama_client_add" name="nama_client_add">
                        </div>
                        <div class="form-group">
                            <label for="usr">Link URL :</label>
                            <input type="text" class="form-control" id="link_url_client_add" name="link_url_client_add">
                        </div>
                        <div class="form-group">
                            <label for="usr">Logo :</label>
                            <input type="file" class="form-control-file" id="logo" name="logo"></input>
                            <i>Maksimal file upload 2MB, dengan format .jpg, .png dan .jpeg</i></br>
                        </div>
                        <div class="form-group">
                            <label for="usr">Status :</label>
                            <select name="client_status_id_add" id="client_status_id_add" class="form-control" required>
                                <option value="">Pilih Status</option>
                                <option value="0">Non Active</option>
                                <option value="1">Active</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-success">Simpan</button>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>

        <div id="editClientSettings" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit Client</h4>
                    </div>

                    <?php echo form_open_multipart('admin/update_client_settings'); ?>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="usr">Nama Client :</label>
                            <input type="hidden" class="form-control" id="client_id_edit" name="client_id_edit">
                            <input type="text" class="form-control" id="nama_client_edit" name="nama_client_edit">
                        </div>
                        <div class="form-group">
                            <label for="usr">Link URL :</label>
                            <input type="text" class="form-control" id="link_url_client_edit" name="link_url_client_edit">
                        </div>
                        <div class="form-group">
                            <label for="usr">Logo :</label>
                            <input type="hidden" class="form-control" id="old_logo" name="old_logo">
                            <input type="file" class="form-control-file" id="logo" name="logo"></input>
                            <i>Maksimal file upload 2MB, dengan format .jpg, .png dan .jpeg</i></br>
                            <br />
                            <span id="logo_image" name="logo_image">
                        </div>
                        <div class="form-group">
                            <label for="usr">Status :</label>
                            <select name="client_status_id_edit" id="client_status_id_edit" class="form-control" required>
                                <option value="">Pilih Status</option>
                                <option value="0">Non Active</option>
                                <option value="1">Active</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-success">Simpan</button>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- #/paper bg -->
</div>
<!-- ./wrap-sidebar-content -->

<!-- / END OF CONTENT -->