<!-- CONTENT -->
<div class="wrap-fluid" id="paper-bg">
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="news-widget">
                        <h2>
                            <span class="bg-red"><?= $menu_title; ?></span>
                        </h2>
                    </div>
                    <span style="font-size: 15px; margin-left: 10px;">HTML/JavaScript (Home)</span><br />
                    <span style="font-size: 13px; margin-left: 10px;">Gadget HTML/JavaScript</span><br />
                    <a href="#" data-toggle="modal" data-target="#edit_team_settings">
                        <p style="font-size: 12px; text-align:right; margin-right: 10px; color:blue;">Edit</p>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="news-widget">
                        <h2>
                            <span class="bg-red"><?= $menu_title; ?></span>
                        </h2>
                    </div>
                    <div class="box-body table-responsive">
                        <?= $this->session->flashdata('message-client'); ?>
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#teamSetting">Tambah Team</button>
                        <br /><br />
                        <table id="dataTeamSetting" class="table table-bordered table-striped" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th style="width: 5%;">#</th>
                                    <th>Nama</th>
                                    <th>Jabatan</th>
                                    <th style="width: 5%;">Twitter</th>
                                    <th style="width: 5%;">Facebook</th>
                                    <th style="width: 5%;">Istagram</th>
                                    <th style="width: 5%;">Linkedin</th>
                                    <th style="width: 18%;">Photo</th>
                                    <th style="width: 10%;">Status</th>
                                    <th style="width: 15%;">Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>

        <div id="teamSetting" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Tambah Team</h4>
                    </div>

                    <?php echo form_open_multipart('admin/add_team_setting'); ?>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="usr">Nama :</label>
                            <input type="text" class="form-control" id="nama_team_add" name="nama_team_add">
                        </div>
                        <div class="form-group">
                            <label for="usr">Jabatan :</label>
                            <input type="text" class="form-control" id="jabatan_team_add" name="jabatan_team_add">
                        </div>
                        <div class="form-group">
                            <label style="font-size: 14px;">Link Sosial Media :</label>
                            <hr />
                            <span><b>Twitter :</b></span>
                            <input type="text" class="form-control" id="sosmed_twitter_add" name="sosmed_twitter_add">
                            <span><b>Facebook :</b></span>
                            <input type="text" class="form-control" id="sosmed_facebook_add" name="sosmed_facebook_add">
                            <span><b>Instagram :</b></span>
                            <input type="text" class="form-control" id="sosmed_instagram_add" name="sosmed_instagram_add">
                            <span><b>Linkedin :</b></span>
                            <input type="text" class="form-control" id="sosmed_linkedin_add" name="sosmed_linkedin_add">
                        </div>
                        <div class="form-group">
                            <label for="usr">Photo :</label>
                            <input type="file" class="form-control-file" id="logo" name="logo"></input>
                            <i>Maksimal file upload 2MB, dengan format .jpg, .png dan .jpeg</i></br>
                        </div>
                        <div class="form-group">
                            <label for="usr">Status :</label>
                            <select name="team_status_id_add" id="team_status_id_add" class="form-control" required>
                                <option value="">Pilih Status</option>
                                <option value="0">Non Active</option>
                                <option value="1">Active</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-success">Simpan</button>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>

        <div id="editTeamSettings" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit Team</h4>
                    </div>

                    <?php echo form_open_multipart('admin/update_team_settings'); ?>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="usr">Nama :</label>
                            <input type="hidden" class="form-control" id="team_id_edit" name="team_id_edit">
                            <input type="text" class="form-control" id="nama_team_edit" name="nama_team_edit">
                        </div>
                        <div class="form-group">
                            <label for="usr">Jabatan :</label>
                            <input type="text" class="form-control" id="jabatan_team_edit" name="jabatan_team_edit">
                        </div>
                        <div class="form-group">
                            <label style="font-size: 14px;">Link Sosial Media :</label>
                            <hr />
                            <span><b>Twitter :</b></span>
                            <input type="text" class="form-control" id="sosmed_twitter_edit" name="sosmed_twitter_edit">
                            <span><b>Facebook :</b></span>
                            <input type="text" class="form-control" id="sosmed_facebook_edit" name="sosmed_facebook_edit">
                            <span><b>Instagram :</b></span>
                            <input type="text" class="form-control" id="sosmed_instagram_edit" name="sosmed_instagram_edit">
                            <span><b>Linkedin :</b></span>
                            <input type="text" class="form-control" id="sosmed_linkedin_edit" name="sosmed_linkedin_edit">
                        </div>
                        <div class="form-group">
                            <label for="usr">Logo :</label>
                            <input type="hidden" class="form-control" id="old_logo" name="old_logo">
                            <input type="file" class="form-control-file" id="logo" name="logo"></input>
                            <i>Maksimal file upload 2MB, dengan format .jpg, .png dan .jpeg</i></br>
                            <br />
                            <span id="logo_photo" name="logo_photo">
                        </div>
                        <div class="form-group">
                            <label for="usr">Status :</label>
                            <select name="team_status_id_edit" id="team_status_id_edit" class="form-control" required>
                                <option value="">Pilih Status</option>
                                <option value="0">Non Active</option>
                                <option value="1">Active</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-success">Simpan</button>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>

        <div id="edit_team_settings" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit Team Settings</h4>
                    </div>

                    <?php echo form_open_multipart('admin/update_title_team_settings'); ?>
                    <div class="modal-body">
                        <?php foreach ($team_settings as $ts) : ?>
                            <div class="form-group">
                                <label for="usr">Title :</label>
                                <input type="text" class="form-control" id="title-team" name="title-team" value="<?= $ts['judul']; ?>">
                            </div>
                            <div class="form-group">
                                <label for="usr">Description :</label>
                                <textarea type="text" class="form-control" id="content-team" name="content-team" style="height: 250px"><?= $ts['content']; ?></textarea>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-success">Simpan</button>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- #/paper bg -->
</div>
<!-- ./wrap-sidebar-content -->

<!-- / END OF CONTENT -->