<!-- CONTENT -->
<div class="wrap-fluid" id="paper-bg">
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="news-widget">
                        <h2>
                            <span class="bg-red"><?= $menu_title; ?></span>
                        </h2>
                    </div>
                    <span style="font-size: 15px; margin-left: 10px;">HTML/JavaScript (Home)</span><br />
                    <span style="font-size: 13px; margin-left: 10px;">Gadget HTML/JavaScript</span><br />
                    <a href="#" data-toggle="modal" data-target="#edit_home_settings">
                        <p style="font-size: 12px; text-align:right; margin-right: 10px; color:blue;">Edit</p>
                    </a>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

    <!-- Modal Footer -->
    <div id="edit_home_settings" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><?= $menu_title; ?></h4>
                </div>

                <?php echo form_open_multipart('admin/update_home_settings'); ?>
                <div class="modal-body">
                    <?php foreach ($home_settings as $hs) : ?>
                        <div class="form-group">
                            <label for="usr">Title :</label>
                            <input type="text" class="form-control" id="title-home" name="title-home" value="<?= $hs['judul']; ?>">
                        </div>
                        <div class="form-group">
                            <label for="usr">Description :</label>
                            <textarea class="form-control" id="content-home" name="content-home" style="height: 250px"><?= $hs['content']; ?></textarea>
                        </div>
                        <div class="form-group">
                            <label for="usr">Logo :</label>
                            <input type="file" class="form-control-file" id="logo" name="logo"></input>
                            <input type="hidden" id="old_logo" name="old_logo" value="<?= $hs['image']; ?>" />
                            <i>Maksimal file upload 2MB, dengan format .jpg, .png dan .jpeg</i></br>
                            <img src="<?php echo base_url('assets/uploads/'); ?><?= $hs['image']; ?>" height="100px" style="margin-top: 10px;">
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success">Simpan</button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<!-- #/paper bg -->
</div>
<!-- ./wrap-sidebar-content -->

<!-- / END OF CONTENT -->