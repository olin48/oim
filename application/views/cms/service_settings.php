<!-- CONTENT -->
<div class="wrap-fluid" id="paper-bg">
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="news-widget">
                        <h2>
                            <span class="bg-red"><?= $menu_title; ?></span>
                        </h2>
                    </div>
                    <span style="font-size: 15px; margin-left: 10px;">HTML/JavaScript (Home)</span><br />
                    <span style="font-size: 13px; margin-left: 10px;">Gadget HTML/JavaScript</span><br />
                    <a href="#" data-toggle="modal" data-target="#edit_service_settings">
                        <p style="font-size: 12px; text-align:right; margin-right: 10px; color:blue;">Edit</p>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="news-widget">
                        <h2>
                            <span class="bg-red"><?= $menu_title; ?></span>
                        </h2>
                    </div>
                    <div class="box-body table-responsive">
                        <?= $this->session->flashdata('message-service'); ?>
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#serviceSetting">Tambah Team</button>
                        <br /><br />
                        <table id="dataServiceSetting" class="table table-bordered table-striped" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th style="width: 5%;">#</th>
                                    <th>Service</th>
                                    <th>Description</th>
                                    <th>Logo</th>
                                    <th>Url</th>
                                    <th>Status</th>
                                    <th style="width: 15%;">Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>

        <div id="serviceSetting" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Tambah Service</h4>
                    </div>

                    <?php echo form_open_multipart('admin/add_service_setting'); ?>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="usr">Service :</label>
                            <input type="text" class="form-control" id="service_add" name="service_add">
                        </div>
                        <div class="form-group">
                            <label for="usr">Description :</label>
                            <textarea class="form-control" id="description_add" name="description_add" style="height: 250px"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="usr">Logo :</label>
                            <input type="file" class="form-control-file" id="logo" name="logo"></input>
                            <i>Maksimal file upload 2MB, dengan format .jpg, .png dan .jpeg</i></br>
                        </div>
                        <div class="form-group">
                            <label for="usr">Url :</label>
                            <input type="text" class="form-control" id="url_add" name="url_add">
                        </div>
                        <div class="form-group">
                            <label for="usr">Status :</label>
                            <select name="service_id_add" id="service_id_add" class="form-control" required>
                                <option value="">Pilih Status</option>
                                <option value="0">Non Active</option>
                                <option value="1">Active</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-success">Simpan</button>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>

        <div id="editServiceSettings" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit Service</h4>
                    </div>

                    <?php echo form_open_multipart('admin/update_service_settings'); ?>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="usr">Service :</label>
                            <input type="hidden" class="form-control" id="id_edit" name="id_edit">
                            <input type="text" class="form-control" id="service_edit" name="service_edit">
                        </div>
                        <div class="form-group">
                            <label for="usr">Description :</label>
                            <textarea class="form-control" id="description_edit" name="description_edit" style="height: 250px"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="usr">Logo :</label>
                            <input type="hidden" class="form-control" id="old_logo" name="old_logo">
                            <input type="file" class="form-control-file" id="logo" name="logo"></input>
                            <i>Maksimal file upload 2MB, dengan format .jpg, .png dan .jpeg</i></br>
                            <br />
                            <span id="logo_photo" name="logo_photo">
                        </div>
                        <div class="form-group">
                            <label for="usr">url :</label>
                            <input type="text" class="form-control" id="url_edit" name="url_edit">
                        </div>
                        <div class="form-group">
                            <label for="usr">Status :</label>
                            <select name="service_id_edit" id="service_id_edit" class="form-control" required>
                                <option value="">Pilih Status</option>
                                <option value="0">Non Active</option>
                                <option value="1">Active</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-success">Simpan</button>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>

        <div id="edit_service_settings" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit Service Settings</h4>
                    </div>

                    <?php echo form_open_multipart('admin/update_title_service_settings'); ?>
                    <div class="modal-body">
                        <?php foreach ($service_settings as $ss) : ?>
                            <div class="form-group">
                                <label for="usr">Title :</label>
                                <input type="text" class="form-control" id="title-team" name="title-team" value="<?= $ss['judul']; ?>">
                            </div>
                            <div class="form-group">
                                <label for="usr">Description :</label>
                                <textarea type="text" class="form-control" id="content-team" name="content-team" style="height: 250px"><?= $ss['content']; ?></textarea>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-success">Simpan</button>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- #/paper bg -->
</div>
<!-- ./wrap-sidebar-content -->

<!-- / END OF CONTENT -->