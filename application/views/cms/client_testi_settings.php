<!-- CONTENT -->
<div class="wrap-fluid" id="paper-bg">
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="news-widget">
                        <h2>
                            <span class="bg-red"><?= $menu_title; ?></span>
                        </h2>
                    </div>
                    <div class="box-body table-responsive">
                        <?= $this->session->flashdata('message-client-testi'); ?>
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#clientTestiSetting">Tambah Client Testimonial</button>
                        <br /><br />
                        <table id="dataClientTestiSetting" class="table table-bordered table-striped" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th style="width: 5%;">#</th>
                                    <th>Nama</th>
                                    <th>Jabatan</th>
                                    <th>Photo</th>
                                    <th>Komentar</th>
                                    <th style="width: 5%;">Status</th>
                                    <th style="width: 10%;">Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>

        <div id="clientTestiSetting" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Tambah Client Testimonial</h4>
                    </div>

                    <?php echo form_open_multipart('admin/add_client_testi_setting'); ?>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="usr">Nama :</label>
                            <input type="text" class="form-control" id="client_testi_add" name="client_testi_add">
                        </div>
                        <div class="form-group">
                            <label for="usr">Jabatan :</label>
                            <input type="text" class="form-control" id="jabatan_testi_add" name="jabatan_testi_add">
                        </div>
                        <div class="form-group">
                            <label for="usr">Photo :</label>
                            <input type="file" class="form-control-file" id="logo" name="logo"></input>
                            <i>Maksimal file upload 2MB, dengan format .jpg, .png dan .jpeg</i></br>
                        </div>
                        <div class="form-group">
                            <label for="usr">Komentar :</label>
                            <textarea class="form-control" id="komentar_testi_add" name="komentar_testi_add"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="usr">Status :</label>
                            <select name="client_testi_status_id_add" id="client_testi_status_id_add" class="form-control" required>
                                <option value="">Pilih Status</option>
                                <option value="0">Non Active</option>
                                <option value="1">Active</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-success">Simpan</button>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>

        <div id="editClientTestiSettings" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit Client Testimonial</h4>
                    </div>

                    <?php echo form_open_multipart('admin/update_client_testi_settings'); ?>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="usr">Nama :</label>
                            <input type="hidden" class="form-control" id="client_testi_id_edit" name="client_testi_id_edit">
                            <input type="text" class="form-control" id="client_testi_edit" name="client_testi_edit">
                        </div>
                        <div class="form-group">
                            <label for="usr">Jabatan :</label>
                            <input type="text" class="form-control" id="jabatan_testi_edit" name="jabatan_testi_edit">
                        </div>
                        <div class="form-group">
                            <label for="usr">Photo :</label>
                            <input type="hidden" class="form-control" id="old_logo" name="old_logo">
                            <input type="file" class="form-control-file" id="logo" name="logo"></input>
                            <i>Maksimal file upload 2MB, dengan format .jpg, .png dan .jpeg</i></br>
                            <br />
                            <span id="photo" name="photo">
                        </div>
                        <div class="form-group">
                            <label for="usr">Komentar :</label>
                            <textarea class="form-control" id="komentar_testi_edit" name="komentar_testi_edit"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="usr">Status :</label>
                            <select name="client_testi_status_id_edit" id="client_testi_status_id_edit" class="form-control" required>
                                <option value="">Pilih Status</option>
                                <option value="0">Non Active</option>
                                <option value="1">Active</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-success">Simpan</button>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- #/paper bg -->
</div>
<!-- ./wrap-sidebar-content -->

<!-- / END OF CONTENT -->