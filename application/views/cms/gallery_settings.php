<!-- CONTENT -->
<div class="wrap-fluid" id="paper-bg">
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="news-widget">
                        <h2>
                            <span class="bg-red"><?= $menu_title; ?></span>
                        </h2>
                    </div>
                    <span style="font-size: 15px; margin-left: 10px;">HTML/JavaScript (Home)</span><br />
                    <span style="font-size: 13px; margin-left: 10px;">Gadget HTML/JavaScript</span><br />
                    <a href="#" data-toggle="modal" data-target="#edit_gallery_settings">
                        <p style="font-size: 12px; text-align:right; margin-right: 10px; color:blue;">Edit</p>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="news-widget">
                        <h2>
                            <span class="bg-red"><?= $menu_title; ?></span>
                        </h2>
                    </div>
                    <div class="box-body table-responsive">
                        <?= $this->session->flashdata('message-gallery'); ?>
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#gallerySetting">Tambah Galley</button>
                        <br /><br />
                        <table id="dataGallerySetting" class="table table-bordered table-striped" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th style="width: 5%;">#</th>
                                    <th>Nama</th>
                                    <th>Keterangan</th>
                                    <th style="width: 18%;">Image</th>
                                    <th style="width: 10%;">Status</th>
                                    <th style="width: 15%;">Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>

        <div id="gallerySetting" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Tambah Galley</h4>
                    </div>

                    <?php echo form_open_multipart('admin/add_gallery_settings'); ?>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="usr">Nama :</label>
                            <input type="text" class="form-control" id="nama_gallery_add" name="nama_gallery_add">
                        </div>
                        <div class="form-group">
                            <label for="usr">Keterangan :</label>
                            <input type="text" class="form-control" id="ket_gallery_add" name="ket_gallery_add">
                        </div>
                        <div class="form-group">
                            <label for="usr">Image :</label>
                            <input type="file" class="form-control-file" id="logo" name="logo"></input>
                            <i>Maksimal file upload 2MB, dengan format .jpg, .png dan .jpeg</i></br>
                        </div>
                        <div class="form-group">
                            <label for="usr">Status :</label>
                            <select name="gallery_status_id_add" id="gallery_status_id_add" class="form-control" required>
                                <option value="">Pilih Status</option>
                                <option value="0">Non Active</option>
                                <option value="1">Active</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-success">Simpan</button>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>

        <div id="editGallerySettings" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit Gallery</h4>
                    </div>

                    <?php echo form_open_multipart('admin/update_gallery_settings'); ?>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="usr">Nama :</label>
                            <input type="hidden" class="form-control" id="gallery_id_edit" name="gallery_id_edit">
                            <input type="text" class="form-control" id="nama_gallery_edit" name="nama_gallery_edit">
                        </div>
                        <div class="form-group">
                            <label for="usr">Keterangan :</label>
                            <input type="text" class="form-control" id="ket_gallery_edit" name="ket_gallery_edit">
                        </div>
                        <div class="form-group">
                            <label for="usr">Image :</label>
                            <input type="hidden" class="form-control" id="old_logo" name="old_logo">
                            <input type="file" class="form-control-file" id="logo" name="logo"></input>
                            <i>Maksimal file upload 2MB, dengan format .jpg, .png dan .jpeg</i></br>
                            <br />
                            <span id="logo_photo" name="logo_photo">
                        </div>
                        <div class="form-group">
                            <label for="usr">Status :</label>
                            <select name="gallery_status_id_edit" id="gallery_status_id_edit" class="form-control" required>
                                <option value="">Pilih Status</option>
                                <option value="0">Non Active</option>
                                <option value="1">Active</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-success">Simpan</button>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>

        <div id="edit_gallery_settings" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit Gallery Settings</h4>
                    </div>

                    <?php echo form_open_multipart('admin/update_title_gallery_settings'); ?>
                    <div class="modal-body">
                        <?php foreach ($gallery_settings as $gs) : ?>
                            <div class="form-group">
                                <label for="usr">Title :</label>
                                <input type="text" class="form-control" id="title-gallery" name="title-gallery" value="<?= $gs['judul']; ?>">
                            </div>
                            <div class="form-group">
                                <label for="usr">Description :</label>
                                <textarea type="text" class="form-control" id="content-gallery" name="content-gallery" style="height: 250px"><?= $gs['content']; ?></textarea>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-success">Simpan</button>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- #/paper bg -->
</div>
<!-- ./wrap-sidebar-content -->

<!-- / END OF CONTENT -->