<!-- CONTENT -->
<div class="wrap-fluid" id="paper-bg">

    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header">
                    <!-- tools box -->
                    <div class="pull-right box-tools">

                        <span class="box-btn" data-widget="collapse"><i class="fa fa-minus"></i>
                        </span>
                    </div>
                    <h3 class="box-title"><i class="fontello-doc"></i>
                        <span>Data <?php echo $menu_title; ?></span>
                    </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <?= $this->session->flashdata('message'); ?>
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal">Tambah <?php echo $menu_title; ?></button>
                    <br /><br />
                    <table id="dataKorwil" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th style="width: 5%;">#</th>
                                <th>Kab/Kota</th>
                                <th>Korwil</th>
                                <th style="width: 10%;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            <?php foreach ($korwil as $kor) { ?>
                                <tr>
                                    <td style="width: 5%;"><?= $i++; ?></td>
                                    <td><?= $kor['nm_kabkota']; ?></td>
                                    <td><?php if ($kor['cd_korwil'] != null) echo $kor['cd_korwil'] . " -"; ?> <?= $kor['nm_korwil']; ?></td>
                                    <td align="center" style="width: 10%;">
                                        <button class="btn-link d-edit" data-toggle="modal" data-target="#modal_edit_korwil_<?= $kor['id']; ?>" title="Edit">
                                            <i class="fa fa-edit" style="color:green"></i>
                                        </button>
                                        &nbsp;
                                        <a title="Delete" href="<?= base_url('management/delete_korwil/' . $kor['id']); ?>" onclick="return confirm('Anda yakin ingin menghapus data ini?')" type="button" class="btn-link d-delete">
                                            <i class="fontello-trash-2" style="color:red"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tambah <?php echo $menu_title; ?></h4>
                </div>
                <form action="<?= base_url('management/korwil'); ?>" method="post">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="usr">Kab/Kota :</label>
                            <select name="kabkota_id" id="kabkota_id" class="form-control" required>
                                <option value="">Pilih Kab/Kota</option>
                                <?php foreach ($kabkota as $kk) : ?>
                                    <option value="<?= $kk['id']; ?>"><?= $kk['nm_kabkota']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="usr">Kode Korwil :</label>
                            <input type="text" class="form-control" id="cd_korwil" name="cd_korwil">
                        </div>
                        <div class="form-group">
                            <label for="usr">Nama Korwil :</label>
                            <input type="text" class="form-control" id="nm_korwil" name="nm_korwil" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-success">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <?php foreach ($korwil as $kor) { ?>
        <div id="modal_edit_korwil_<?= $kor['id'] ?>" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit <?php echo $menu_title; ?></h4>
                    </div>
                    <form action="<?= base_url('management/edit_korwil/'); ?><?= $kor['id'] ?>" method="post">
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="usr">Kab/Kota :</label>
                                <select name="kabkota_id" id="kabkota_id" class="form-control" required>
                                    <option value="">Pilih Kab/Kota</option>
                                    <?php foreach ($kabkota as $kk) : ?>
                                        <option <?php if ($kor['kabkota_id'] == $kk['id']) {
                                                    echo "selected=selected";
                                                } ?> value="<?= $kk['id']; ?>"><?= $kk['nm_kabkota']; ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="usr">Kode Korwil :</label>
                                <input type="text" class="form-control" id="cd_korwil" name="cd_korwil" value="<?= $kor['cd_korwil']; ?>">
                            </div>
                            <div class="form-group">
                                <label for="usr">Nama Korwil :</label>
                                <input type="text" class="form-control" id="nm_korwil" name="nm_korwil" value="<?= $kor['nm_korwil']; ?>" required>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-success">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    <?php } ?>

</div>
<!-- #/paper bg -->
</div>
<!-- ./wrap-sidebar-content -->

<!-- / END OF CONTENT -->